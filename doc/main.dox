/*! \mainpage GWOLLUM
 * \tableofcontents
 * The GWOLLUM package contains a set of software tools designed to perform gravitational-wave analyses:
 * - Utilities: C++ utilities, Fourier transform interface, plotting interface, signal processing and filtering...
 * - Time segment management (Segments class)
 * - GW frame data interface (ffl class)
 * - Network of gravitational-wave detectors
 * - Software injections of signal waveforms in the data (InjEct class)
 * - Triggers management (MakeTriggers and ReadTriggers classes)
 * - Coincidence between triggers (Coinc2 class)
 *
 * The source code of GWOLLUM is written with the C++ language.
 * It relies on the CERN [ROOT](http://root.cern.ch) libraries (also C++).
 *
 * \section main_program_sec User programs
 * GWOLLUM comes with a collection of user programs using GWOLLUM libraries:
 * - gwl-print-channels.cc: this program prints the list of channels contained in GW frame files.
 * - gwl-print-detector-response.cc: this program prints the detector response (antenna pattern) to a gravitational wave.
 * - gwl-print-psd.cc: this program plots the noise power/amplitude spectrum of a time series.
 * - gwl-print-livetime.cc: this program prints the processed time segments of trigger files.
 * - gwl-txt-to-frame.cc: this program generates GW frame files using a time series saved in a text file.
 * - gwl-trigger-merge.cc: this program merges multiple trigger ROOT files into one single file.
 * - gwl-txt-to-tree.cc: this program generates a ROOT TTree structure in a ROOT file using parameters saved in a text file.
 * - gwl-txt-to-triggers.cc: this program converts a list of triggers from a txt file to a ROOT file.
 *
 * \section main_develop_sec Develop a project on top of GWOLLUM libraries
 * GWOLLUM offers a set of [C++ classes](annotated.html) to run gravitational-wave analyses. The corresponding libraries can be used to develop user programs or projects:
 * - [C++ classes](annotated.html): documentation for GWOLLUM C++ classes.
 *
 * \section main_install_sec Install GWOLLUM (conda)
 * Install GWOLLUM from the `conda-forge` channel:
 * \verbatim
conda create -c conda-forge --name gwollum gwollum
\endverbatim
 * Simply activate your `gwollum` environment to use GWOLLUM libraries and programs: 
 * \verbatim
conda activate gwollum
 \endverbatim
 *
 * \section main_build_sec Build GWOLLUM from sources
 * First, define your source and installation directories. For example:
 * \verbatim
export GWOLLUM_SRCDIR=${HOME}/src
export GWOLLUM_INSTALLDIR=${HOME}/opt/GWOLLUM
mkdir -p ${GWOLLUM_SRCDIR} ${GWOLLUM_INSTALLDIR}
 \endverbatim
 * You can download the GWOLLUM source tarball from [gitlab](https://git.ligo.org/virgo/virgoapp/GWOLLUM/-/releases):
 * \verbatim
 cd ${GWOLLUM_SRCDIR}
 wget https://git.ligo.org/virgo/virgoapp/GWOLLUM/-/archive/[X.Y.Z]/GWOLLUM-[X.Y.Z].tar.gz
 tar -xzf GWOLLUM-[X.Y.Z].tar.gz
 \endverbatim
 * where `[X.Y.Z]` is the GWOLLUM version of your choice.
 *
 * Alternatively, you can get a development copy of the GWOLLUM software using git and select a release tag:
 * \verbatim
 cd ${GWOLLUM_SRCDIR}
 git clone https://git.ligo.org/virgo/virgoapp/GWOLLUM.git
 cd GWOLLUM/
 git checkout [X.Y.Z]
 \endverbatim
 *
 * The GWOLLUM package relies on several external packages which you must install on your machine:
 * - [ROOT](http://root.cern.ch) libraries are used for plotting and data management. ROOT (version 6) must be installed. The ROOT environment must be sourced with `${ROOTSYS}/bin/thisroot.sh`.
 * - The [FFTW](http://www.fftw.org/) libraries are required to perform fast Fourier transforms.
 * - The [HDF5](https://www.hdfgroup.org/solutions/hdf5/) C++ libraries are required to support the hdf5 file format.
 * - The [Fr](https://git.ligo.org/virgo/virgoapp/Fr) package must be installed to supprt the GW frame file format.
 * - [CMake](https://cmake.org/) is used to build GWOLLUM.
 * - [Doxygen](http://www.doxygen.nl/) is used to build the GWOLLUM documentation. This package is optional.
 *
 * Here, we give step-by-step instructions to build GWOLLUM for UNIX (bash/sh) systems.
 \verbatim
# go to the source directory
cd ${GWOLLUM_SRCDIR}/GWOLLUM/

# create the build directory
mkdir ./build/; cd ./build/

# configure GWOLLUM
cmake -DCMAKE_INSTALL_PREFIX=${GWOLLUM_INSTALLDIR} ${GWOLLUM_SRCDIR}/GWOLLUM

# compile GWOLLUM
make

# install GWOLLUM
make install
\endverbatim
 * An environment script has been generated. It must be sourced before using GWOLLUM:
 * \verbatim
source ${GWOLLUM_INSTALLDIR}/etc/gwollum.env.sh
\endverbatim
 * You can test that GWOLLUM is correctly installed by typing:
 * \verbatim
gwl-print-channels version
\endverbatim
 *
 * If you have the [Doxygen](http://www.doxygen.nl/) software installed in your machine, you can access GWOLLUM documentation with a web browser at `file://${GWOLLUM_INSTALLDIR}/share/doc/GWOLLUM/html/index.html`.
 *
 * \section main_documentation_sec Documentation and links
 *
 * \subsection main_documentation_general_sec General
 *
 * - [GWOLLUM gitlab page](https://git.ligo.org/virgo/virgoapp/GWOLLUM): this is the code repository.
 * - [GWOLLUM wiki](https://git.ligo.org/virgo/virgoapp/GWOLLUM/-/wikis/home): additional and specific documentation is provided there.
 * - [GWOLLUM issue tracker](https://git.ligo.org/virgo/virgoapp/GWOLLUM/-/issues): use this link to report bugs, request new features...
 * - [Laboratoire de Physique des 2 infinis Irène Joliot-Curie (IJCLab)](https://www.ijclab.in2p3.fr)
 *  
 * \subsection main_documentation_related_sec Related pages
 *
 * See all documentation pages in the [Related Pages](./pages.html)
 *
 * \section main_author_sec Authors
 * <b>Florent Robinet</b> - Laboratoire de Physique des 2 Infinis Irène Joliot-Curie (IJCLab) Orsay, France - <a href="mailto:florent.robinet@ijclab.in2p3.fr">contact</a>
 */

