/**
 * @file 
 * @brief Read a set of injections.
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#ifndef __InjRea__
#define __InjRea__

#include <TChain.h>
#include <TTreeIndex.h>
#include <TH1.h>
#include <TGraph.h>
#include <TMath.h>
#include "InjTyp.h"

using namespace std;

/**
 * @brief Read a set of injections.
 * @details Injection parameters, generated with the InjGen class, are loaded.
 * @author Florent Robinet
 */
class InjRea{

 public:
  
  /**
   * @name Constructors and destructors
   @{
  */
  /**
   * @brief Constructor of the InjRea class.
   * @details Injections in ROOT files designated by a file pattern are loaded.
   * Each injection is tagged to 'true' by default.
   * Injection files must be generated with the InjGen class.
   * @param aPattern Injection file pattern.
   * @param aVerbose Verbosity level.
   */
  InjRea(const string aPattern, const unsigned int aVerbose=0);

  /**
   * @brief Destructor of the InjRea class.
   */
  virtual ~InjRea();
  /**
     @}
  */
  
  /**
   * @brief Returns the index of the current injection.
   */
  inline Long64_t GetInjectionIndex(void){ return inj_index; };

  /**
   * @brief Loads a given injection.
   * @returns The function returns the number of bytes read from the input buffer.
   * If entry does not exist or if an I/O error occurs, the function returns 0.
   * @param[in] aInjIndex Injection index.
   */
  int LoadInjection(const Long64_t aInjIndex);

  /**
   * @brief Returns the SineGauss \f$h_{+rss}\f$ peak amplitude of current injection.
   * @details For details see K. Riles, LIGO-T040055-00
   */
  double GetSineGaussh0plus(void);

  /**
   * @brief Returns the SineGauss \f$h_{\times rss}\f$ peak amplitude of current injection.
   * @details For details see K. Riles, LIGO-T040055-00
   */
  double GetSineGaussh0cross(void);

  /**
   * @brief Returns the injection type.
   */
  inline injtype GetInjectionType(void){ return (injtype)wave_type; };

  /**
   * @brief Returns the injection GPS time [s].
   */
  inline double GetInjectionTime(void){ return inj_time; };

  /**
   * @brief Returns the injection right-ascension [rad].
   */
  inline double GetInjectionRa(void){ return inj_ra; };

  /**
   * @brief Returns the injection declination [rad].
   */
  inline double GetInjectionDec(void){ return inj_dec; };

  /**
   * @brief Returns the injection eccentricity.
   */
  inline double GetInjectionEccentricity(void){ return inj_ecc; };

  /**
   * @brief Returns the injection polarization [rad].
   */
  inline double GetInjectionPolarization(void){ return inj_psi; };

  /**
   * @brief Returns the injection amplitude.
   */
  inline double GetInjectionAmplitude(void){ return inj_amp; };

  /**
   * @brief Returns the minimum injection amplitude.
   */
  inline double GetInjectionAmplitudeMin(void){ return inj_ampmin; };

  /**
   * @brief Returns the maximum injection amplitude.
   */
  inline double GetInjectionAmplitudeMax(void){ return inj_ampmax; };
  
  /**
   * @brief Returns the injection sigma [s].
   */
  inline double GetInjectionSigma(void){ return inj_sigma; };

  /**
   * @brief Returns the minimum injection sigma [s].
   */
  inline double GetInjectionSigmaMin(void){ return inj_sigmamin; };

  /**
   * @brief Returns the maximum injection sigma [s].
   */
  inline double GetInjectionSigmaMax(void){ return inj_sigmamax; };
  
  /**
   * @brief Returns the injection frequency [Hz].
   */
  inline double GetInjectionFrequency(void){ return inj_f0; };

  /**
   * @brief Returns the minimum injection frequency [Hz].
   */
  inline double GetInjectionFrequencyMin(void){ return inj_f0min; };

  /**
   * @brief Returns the maximum injection frequency [Hz].
   */
  inline double GetInjectionFrequencyMax(void){ return inj_f0max; };

  /**
   * @brief Returns the GPS starting time of the injection [s] at the center of the Earth.
   * @details It depends on the type of waveform:
   * - Sine-Gaussian waveform: this is 10 sigmas before the injection time.
   * - User-defined waveform: start time of the \f$h_{+}\f$ time series relatively to the injection time.
   * @returns 0 if the waveform type is unknown.
   */
  double GetInjectionTimeStart(void);

  /**
   * @brief Returns the GPS ending time of the injection [s] at the center of the Earth.
   * @details It depends on the type of waveform:
   * - Sine-Gaussian waveform: this is 10 sigmas after the injection time.
   * - User-defined waveform: end time of the \f$h_{+}\f$ time series relatively to the injection time.
   * @returns 0 if the waveform type is unknown.
   */
  double GetInjectionTimeEnd(void);

  /**
   * @brief Returns the injection tag.
   */
  inline bool GetInjectionTag(void){ return inj_tag[InjTree->GetReadEntry()]; };

  /**
   * @brief Sets a new tag value to the current injection.
   * @param[in] aTag New tag value.
   */
  inline void SetInjectionTag(const bool aTag){ inj_tag[InjTree->GetReadEntry()]=aTag; };

  /**
   * @brief Returns the number of injections.
   */
  inline Long64_t GetN(void){ return InjTree->GetEntries(); };
  
  /**
   * @brief Returns the 1D distribution of an injection parameter.
   * @note The returned histograms must be deleted by the user.
   * @warning Only injections tagged to true are considered.
   * @param[in] aParamName Parameter name. Currently supported: "amplitude" and "f0".
   * @param[in] aNbins Number of bins (must be >0).
   * @param[in] aBinType Binning type. Currently supported: "UNIFORM" and "LOG".
   */
  TH1D* GetInjectionParamDist(const string aParamName, const unsigned int aNbins=1, const string aBinType="UNIFORM");

  /**
   * @brief Returns the input file pattern provided in the constructor.
   */
  inline string GetInputFilePattern(void){ return pattern; };

protected:
  
  unsigned int Verbose; ///< Verbosity level.
  TGraph *wave_hplus;   ///< Waveform \f$h_{+}(t)\f$.
  TGraph *wave_hcross;  ///< Waveform \f$h_{\times}(t)\f$.

 private:
  
  string pattern;       ///< Input file pattern.

  // INJECTION TREE
  Long64_t inj_index;   ///< Current injection index.
  TChain *InjTree;      ///< Injection tree.
  TTreeIndex *sort_index;///< TTree index.
  double inj_ra;        ///< Injection right ascension.
  double inj_dec;       ///< Injection declination.
  double inj_psi;       ///< Injection polarization angle.
  double inj_time;      ///< Injection time.
  double inj_amp;       ///< Injection amplitude.
  double inj_f0;        ///< Injection frequency.
  double inj_sigma;     ///< Injection sigma.
  double inj_ecc;       ///< Injection eccentricity.

  // MIN/MAX
  double inj_sigmamin;  ///< Sigma min.
  double inj_sigmamax;  ///< Sigma max.
  double inj_ampmin;    ///< Amplitude min.
  double inj_ampmax;    ///< Amplitude max.
  double inj_f0min;     ///< Frequency min.
  double inj_f0max;     ///< Frequency max.
 
  bool *inj_tag;        ///< Injection tags.
 
  // WAVEFORM TREE
  TChain *WaveTree;     ///< Waveform tree.
  UInt_t wave_type;     ///< Waveform type: see InjTyp.h.

  ClassDef(InjRea,0)
};

#endif


