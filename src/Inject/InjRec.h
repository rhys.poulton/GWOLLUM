//////////////////////////////////////////////////////////////////////////////
//  Author : florent robinet (LAL - Orsay): robinet@lal.in2p3.fr
//////////////////////////////////////////////////////////////////////////////
#ifndef __InjRec__
#define __InjRec__

#include "TObject.h"
#include "EventMap.h"
#include "InjRea.h"

using namespace std;

/**
 * Recover injections in a trigger set.
 * \author    Florent Robinet
 */
class InjRec : public InjRea, public EventMap {
  friend class InjCoi;

 public:
  
  /**
   * @name Constructors and destructors
   @{
  */
  /**
   * Constructor of the InjRec class.
   * Triggers and injections are loaded (see parent classes). Triggers must be clustered as injections will only searched in the cluster sample, see Recover().
   *
   * Injections are discarded (tagged to false, see InjRea::SetInjectionTag()) if:
   * - the detector local time of the injection peak is out of the trigger segments.
   * - the frequency of the injection peak is out of the trigger search band.
   * The remaining injections are said to be 'Valid'. The valid injection set can be changed with InjRea::SetInjectionTag().
   * @param aTriggerFilePattern trigger file pattern
   * @param aInjFilePattern injection file pattern
   * @param aVerbose verbosity level
   */
  InjRec(const string aTriggerFilePattern, const string aInjFilePattern, const int aVerbose=0);

  /**
   * Destructor of the InjRec class.
   */
  virtual ~InjRec(void);
  /**
     @}
  */

  
  /**
   * Recovers injections in the trigger set.
   * Triggers must be clustered first, use Clusterize().
   * Returns the number of found injections. Returns -1 if the function fails in some way.
   * If an injection index is given in argument, information is printed to track the injection through the recovering process.
   *
   * IMPORTANT1: injections tagged to 'false' are not valid and discarded. See InjRea::SetInjectionTag().
   * @param aInjIndex injection index to follow-up
   * @param aClusterTagThr only clusters tagged with a value greater than this parameter are considered in the recovery procedure
   */
  int Recover(const int aInjIndex=-1, const int aClusterTagThr=0);

   /**
   * Draws diagnostic plots.
   * Many diagnostic plots are avalaible and can be drawn with this function. Plots are indexed:
   * - index=0: injection amplitude distribution of valid injections
   * - index=1: recovery efficiency as a function of the injection amplitude
   * - index=2: injection frequency distribution of valid injections
   * - index=3: recovery efficiency as a function of the injection frequency
   * - index=4: recovered amplitude vs injected amplitude
   * - index=5: recovered frequency vs injected frequency
   *
   * A bit of semantic (using amplitude as an example):
   * - injection amplitude: signal amplitude at the source
   * - injected amplitude: signal amplitude injected in the detector's data
   * - recovered amplitude: signal amplitude recovered from the input triggers
   * @param aPlotIndex plot index
   * @param aOptions draw options (see GwollumPlot::Draw())
   * @param aPadIndex pad index (see GwollumPlot::Draw())
   */
  void Draw(const int aPlotIndex, const string aOptions="", const int aPadIndex=0);
  
   /**
   * Prints missed injections above a given amplitude threshold.
   * This function only works if called after Recover().
   * @param aAmplitudeThr amplitude threshold
   */
  void PrintMissedInjection(const double aAmplitudeThr=0.0);

   /**
   * Returns the number of valid injections.
   * An injection is said to be valid if it overlaps the parameter space covered by the trigger set (in time and frequency).
   * This function only works if called after Recover().
   */
  inline int GetNValidInjections(void){
    return ValidInjTree->GetEntries();
  }

   /**
   * Returns the number of recovered injections.
   * This function only works if called after Recover().
   */
  inline int GetNRecInjections(void){
    return (int)rec_injindex.size();
  }

   /**
   * Returns the list of recovered injection.
   * The returned vector contains a list of injection idexes.
   */
  inline vector <int> GetRecInjections(void){
    return rec_injindex;
  }

   /**
   * Returns the number missed injections.
   * This function only works if called after Recover().
   */
  inline int GetNMissedInjections(void){
    return (int)mis_injindex.size();
  }
  
  /**
   * Returns the injected amplitude.
   */
  inline double GetInjectedAmplitude(void){
    double tlocal=GetLocalTime(inj_ra,inj_dec,inj_time);
    double fplus, fcross;
    GetDetAMResponse(fplus, fcross, inj_ra, inj_dec, inj_psi, GreenwichMeanSiderealTime(tlocal));
    return fplus*GetSineGaussh0plus()+fcross*GetSineGaussh0cross();
  };


 private:
  
  TTree *ValidInjTree;       ///< valid injection tree
  int injindex;              ///< mapping with injection tree

  vector <int> rec_injindex; ///< recovered injection index
  vector <int> rec_clindex;  ///< recovered cluster index
  vector <int> mis_injindex; ///< missed injection index

  TH1D *h_inj_amp;           ///< injection amplitude distribution
  TH1D *h_inj_f0;            ///< injection frequency distribution

  TH1D *h1_rec;              ///< TH1D container
  TH2D *h2_rec;              ///< TH2D container
  TGraph *g_rec;             ///< TGraph container

  // make diagnostic plots
  void make_effamp(void);
  void make_efff0(void);
  void make_ampamp(void);
  void make_f0f0(void);

  ClassDef(InjRec,0)
};

#endif


