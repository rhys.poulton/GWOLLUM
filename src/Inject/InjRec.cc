//////////////////////////////////////////////////////////////////////////////
//  Author : florent robinet (LAL - Orsay): robinet@lal.in2p3.fr
//////////////////////////////////////////////////////////////////////////////
#include "InjRec.h"

ClassImp(InjRec)


/////////////////////////////////////////////////////////////////////////
InjRec::InjRec(const string aTriggerFilePattern, const string aInjFilePattern, const int aVerbose) : InjRea(aInjFilePattern,aVerbose), EventMap(1,aTriggerFilePattern,"",aVerbose){ 
/////////////////////////////////////////////////////////////////////////

  // copy injection tree header
  ValidInjTree = InjTree->CloneTree(0);
  ValidInjTree->Branch("injindex", &injindex, "injindex/I");

  // rec containers
  h1_rec = new TH1D();
  h2_rec = new TH2D();
  g_rec = new TGraph();

  // select valid injection
  if(InjRea::Verbose) cout<<"InjRec::InjRec: Select valid injections for this trigger stream..."<<endl;
  double tlocal;
  for(int i=0; i<GetNInjections(); i++){
    LoadInjection(i);
                
    // remove out-of-time injections
    tlocal=GetLocalTime(inj_ra,inj_dec,inj_time);
    if(!Seg->IsInsideSegment(tlocal)) SetInjectionTag(false);
    
    // remove out-of-band injections
    if(inj_f0){
      if(inj_f0<GetFrequencyMin()) SetInjectionTag(false);
      if(inj_f0>=GetFrequencyMax()) SetInjectionTag(false);
    }
    injindex=i;
    if(GetInjectionTag()) ValidInjTree->Fill();
  }
  if(InjRea::Verbose>1) cout<<"InjRec::InjRec: number of valid injections = "<<ValidInjTree->GetEntries()<<" for "<<GetNamePrefix()<<" trigger stream"<<endl;

  // retrieve distributions
  h_inj_amp = GetInjectionParamDist("amplitude",100,"LOG");
  h_inj_f0 = GetInjectionParamDist("f0",100,"LOG");
  
  SetGridx(1);  SetGridy(1);
}

/////////////////////////////////////////////////////////////////////////
InjRec::~InjRec(void){
/////////////////////////////////////////////////////////////////////////
  if(InjRea::Verbose>1) cout<<"InjRec::~InjRec"<<endl;
  delete ValidInjTree;
  mis_injindex.clear();
  rec_injindex.clear();
  rec_clindex.clear();
  delete h_inj_amp;
  delete h_inj_f0;
  delete h1_rec;
  delete h2_rec;
  delete g_rec;
}


/////////////////////////////////////////////////////////////////////////
int InjRec::Recover(const int aInjIndex, const int aClusterTagThr){
/////////////////////////////////////////////////////////////////////////

  double tlocal;
  int cstart=0;
  int n;

  if(InjRea::Verbose) cout<<"InjRec::Recover: recover injections in "<<GetNamePrefix()<<" trigger stream"<<endl;

  // reset
  rec_injindex.clear();
  rec_clindex.clear();
  mis_injindex.clear();
  h_inj_amp->Reset();
  h_inj_f0->Reset();
  ValidInjTree->Reset();
  
  // loop over injections
  for(int i=0; i<GetNInjections(); i++){
    LoadInjection(i);

    if(i==aInjIndex){
      cout<<">>>> Following injection #"<<aInjIndex<<endl;
      cout<<">>>> geocentric time = "<<fixed<<inj_time<<endl;
    }

    // only keep tagged injections
    if(i==aInjIndex) cout<<">>>> Is this injection tagged as Valid?"<<endl;
    if(!GetInjectionTag()){
      if(i==aInjIndex) cout<<">>>> |__ NO!"<<endl;
      continue;
    }
    if(i==aInjIndex) cout<<">>>> |__ YES!"<<endl;
    injindex=i;
    ValidInjTree->Fill();
  
    // get local time
    tlocal=GetLocalTime(inj_ra,inj_dec,inj_time);

    // distribution plots
    h_inj_amp->Fill(inj_amp);
    h_inj_f0->Fill(inj_f0);

    // missed
    mis_injindex.push_back(i);
    n=(int)rec_injindex.size();

    // injection follow-up
    if(i==aInjIndex) cout<<">>>> cluster match:"<<endl;

    // loop over clusters
    for(int c=cstart; c<GetNClusters(); c++){
      if(cl_tag[c]<=aClusterTagThr) continue;
      cstart=c;// for next round

      // injection follow-up
      if(i==aInjIndex) cout<<"          test cluster #"<<c<<", timing = "<<GetClusterTimeStart(c)<<"-"<<GetClusterTimeEnd(c)<<endl;
     
      // time matching
      if(GetClusterTimeEnd(c)<tlocal) continue;
      if(GetClusterTimeStart(c)>=tlocal) break;
      
      // injection is matched with a cluster

      // injection follow-up
      if(i==aInjIndex) cout<<"                         |__ this is a match!"<<endl;

      // this injection was already matched by a previous cluster
      if(rec_injindex.size()&&rec_injindex.back()==i){
	if(fabs(GetClusterTime(c)-tlocal)<fabs(GetClusterTime(rec_clindex.back())-tlocal)){// this new match is better
	  rec_clindex.pop_back();
	  rec_clindex.push_back(c);
	  if(i==aInjIndex) cout<<"                            |__ this is a BETTER match!"<<endl;
	}
      }

      // this injection was never matched before
      else{
	rec_injindex.push_back(i);
	rec_clindex.push_back(c);
	if(i==aInjIndex) cout<<"                            |__ this is the FIRST match!"<<endl;
      }

    }

    if((int)rec_injindex.size()>n){
      mis_injindex.pop_back();
      if(i==aInjIndex) cout<<">>>> the injection is recovered"<<endl;
    }
    else{
      if(i==aInjIndex) cout<<">>>> the injection is missed"<<endl;
    }

  }
  
  if(InjRea::Verbose>1){
    cout<<"InjRec::Recover: "<<ValidInjTree->GetEntries()<<" valid injections for "<<GetNamePrefix()<<" trigger stream"<<endl;
    cout<<"                 "<<rec_injindex.size()<<" of which were recovered"<<endl;
  }

  return (int)rec_injindex.size();
}

/////////////////////////////////////////////////////////////////////////
void InjRec::Draw(const int aPlotIndex, const string aOptions, const int aPadIndex){
/////////////////////////////////////////////////////////////////////////

  if(aPlotIndex==0){ // injection amplitude distribution
    GwollumPlot::Draw(h_inj_amp,aOptions,aPadIndex);
    SetLogx(1); SetLogy(0);
  }
  else if(aPlotIndex==1){ // efficiency vs amplitude
    make_effamp();
    GwollumPlot::Draw(h1_rec,aOptions,aPadIndex);
    SetLogx(1); SetLogy(0);
  }
  if(aPlotIndex==2){ // injection frequency distribution
    GwollumPlot::Draw(h_inj_f0,aOptions,aPadIndex);
    SetLogx(1); SetLogy(0);
  }
  else if(aPlotIndex==3){ // efficiency vs frequency
    make_efff0();
    GwollumPlot::Draw(h1_rec,aOptions,aPadIndex);
    SetLogx(1); SetLogy(0);
  }
  else if(aPlotIndex==4){ // recovered vs injected amplitude
    make_ampamp();
    GwollumPlot::Draw(g_rec,aOptions,aPadIndex);
    SetLogx(1); SetLogy(1);
  }
  else if(aPlotIndex==5){ // recovered vs injected frequency
    make_f0f0();
    GwollumPlot::Draw(g_rec,aOptions,aPadIndex);
    SetLogx(1); SetLogy(1);
  }
  else;

  return;
}


/////////////////////////////////////////////////////////////////////////
void InjRec::make_effamp(void){
/////////////////////////////////////////////////////////////////////////
  delete h1_rec;
  h1_rec = (TH1D*)h_inj_amp->Clone("h_eff_amp"); h1_rec->Reset();// FIXME
  h1_rec->SetTitle("Recovery Efficiency vs injection amplitude");
  h1_rec->GetYaxis()->SetTitle("Recovery efficiency");

  for(int r=0; r<(int)rec_injindex.size(); r++){
    LoadInjection(rec_injindex[r]);
    h1_rec->Fill(inj_amp);
  }

  h1_rec->Divide(h_inj_amp);
  return;
}

/////////////////////////////////////////////////////////////////////////
void InjRec::make_efff0(void){
/////////////////////////////////////////////////////////////////////////
  delete h1_rec;
  h1_rec = (TH1D*)h_inj_f0->Clone("h_eff_freq"); h1_rec->Reset();// FIXME
  h1_rec->SetName("h_eff_freq");
  h1_rec->SetTitle("Recovery Efficiency vs injection frequency");
  h1_rec->GetYaxis()->SetTitle("Recovery efficiency");

  for(int r=0; r<(int)rec_injindex.size(); r++){
    LoadInjection(rec_injindex[r]);
    h1_rec->Fill(inj_f0);
  }

  h1_rec->Divide(h_inj_f0);
  return;
}

/////////////////////////////////////////////////////////////////////////
void InjRec::make_ampamp(void){
/////////////////////////////////////////////////////////////////////////
  delete g_rec;
  g_rec = new TGraph((int)rec_injindex.size());
  g_rec->SetName("g_ampamp");
  g_rec->SetTitle("Recovered vs injected amplitude");
  g_rec->GetXaxis()->SetTitle("Injected amplitude");
  g_rec->GetYaxis()->SetTitle("Recovered amplitude");
  g_rec->SetMarkerStyle(6);

  double tlocal, fplus, fcross;
  for(int r=0; r<(int)rec_injindex.size(); r++){
    LoadInjection(rec_injindex[r]);
    tlocal=GetLocalTime(inj_ra,inj_dec,inj_time);
    GetDetAMResponse(fplus, fcross, inj_ra, inj_dec, inj_psi, GreenwichMeanSiderealTime(tlocal));
    g_rec->SetPoint(r,fplus*GetSineGaussh0plus()+fcross*GetSineGaussh0cross(),GetClusterAmplitude(rec_clindex[r]));
  }
  return;
}

/////////////////////////////////////////////////////////////////////////
void InjRec::make_f0f0(void){
/////////////////////////////////////////////////////////////////////////
  delete g_rec;
  g_rec = new TGraph((int)rec_injindex.size());
  g_rec->SetName("g_f0f0");
  g_rec->SetTitle("Recovered vs injected frequency");
  g_rec->GetXaxis()->SetTitle("Injected frequency");
  g_rec->GetYaxis()->SetTitle("Recovered frequency");
  g_rec->SetMarkerStyle(6);

  for(int r=0; r<(int)rec_injindex.size(); r++){
    LoadInjection(rec_injindex[r]);
    g_rec->SetPoint(r,inj_f0,GetClusterFrequency(rec_clindex[r]));
  }
  return;
}

/////////////////////////////////////////////////////////////////////////
void InjRec::PrintMissedInjection(const double aAmplitudeThr){
/////////////////////////////////////////////////////////////////////////
  double fplus, fcross, tlocal;
  for(int m=0; m<(int)mis_injindex.size(); m++){
    LoadInjection(mis_injindex[m]);
    if(inj_amp<aAmplitudeThr) continue;
    tlocal=GetLocalTime(inj_ra,inj_dec,inj_time);
    GetDetAMResponse(fplus, fcross, inj_ra, inj_dec, inj_psi, GreenwichMeanSiderealTime(tlocal));
    cout<<"----------------------------------------------------------------"<<endl;
    cout<<"  index        = "<<mis_injindex[m]<<endl;
    cout<<"  time         = "<<fixed<<inj_time<<"(geocentric), "<<tlocal<<" (local)"<<endl;
    cout<<"  amplitude    = "<<scientific<<inj_amp<<", "<<GetInjectedAmplitude()<<" (injected)"<<endl;
    cout<<"  frequency    = "<<inj_f0<<" Hz"<<endl;
    cout<<"  sigma        = "<<inj_sigma<<" s"<<endl;
    cout<<"  polarization = "<<inj_psi<<endl;
    cout<<"  eccentricity = "<<inj_ecc<<endl;
    cout<<"  sky position = "<<inj_ra<<"/"<<inj_dec<<" (ra/dec)"<<endl;
    cout<<"  antenna      = "<<fplus<<"/"<<fcross<<" (F+/Fx)"<<endl;
  }

  return;
}

