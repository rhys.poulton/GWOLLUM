# -- Inject library ----------

set(
  GWOLLUM_INJECT_HEADERS
  InjTyp.h
  InjTre.h
  InjGen.h
  InjRea.h
  InjEct.h
  #InjCoi.h
  #InjRec.h
  )

# build ROOT dictionary
include_directories(
  #${CMAKE_CURRENT_SOURCE_DIR}/../Coinc
  ${CMAKE_CURRENT_SOURCE_DIR}/../Segments
  ${CMAKE_CURRENT_SOURCE_DIR}/../Streams
  ${CMAKE_CURRENT_SOURCE_DIR}/../Time
  ${CMAKE_CURRENT_SOURCE_DIR}/../Triggers
  ${CMAKE_CURRENT_SOURCE_DIR}/../Utils
  ${HDF5_INCLUDE_DIRS}
  )
root_generate_dictionary(
  InjectDict
  LINKDEF LinkDef.h
  MODULE Inject
  OPTIONS ${GWOLLUM_INJECT_HEADERS}
  )

# compile library
add_library(
  Inject
  SHARED
  InjTre.cc
  InjGen.cc
  InjRea.cc
  InjEct.cc
  #InjCoi.cc
  #InjRec.cc
  InjectDict.cxx
  )
target_include_directories(
  Inject
  PUBLIC
  ${CMAKE_CURRENT_SOURCE_DIR}
  )
target_link_libraries(
  Inject
  CUtils
  RootUtils
  Streams
  Time
  ${ROOT_Core_LIBRARY}
  ${ROOT_MathCore_LIBRARY}
  ${ROOT_TreePlayer_LIBRARY}
  )
set_target_properties(
  Inject PROPERTIES
  PUBLIC_HEADER "${GWOLLUM_INJECT_HEADERS}"
  )

# -- tests -----------
add_executable(
  gwl-test-inject
  gwl-test-inject.cc
  )
target_link_libraries(
  gwl-test-inject
  Inject
  )
add_test(
  NAME gwl-test-inject
  COMMAND gwl-test-inject
  )

# -- installation -----------

# install library
install(
  TARGETS
  Inject
  RUNTIME DESTINATION ${RUNTIME_DESTINATION}
  LIBRARY DESTINATION ${LIBRARY_DESTINATION}
  ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
  PUBLIC_HEADER DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}
  )

# install ROOT PCM
install(
  FILES
  ${CMAKE_CURRENT_BINARY_DIR}/libInject_rdict.pcm
  ${CMAKE_CURRENT_BINARY_DIR}/libInject.rootmap
  DESTINATION ${CMAKE_INSTALL_LIBDIR}
  )

# install test executable(s)
install(
  TARGETS
  gwl-test-inject
  DESTINATION ${CMAKE_INSTALL_SBINDIR}/
  )
