/**
 * @file 
 * @brief Program to test the injection classes.
 *
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "InjGen.h"
#include "InjEct.h"

using namespace std;

/**
 * @brief Test program.
 */
int main(void){

  // test directory
  if(!filesystem::create_directory("./gwl-test-inject-dir")) cerr<<"Cannot create directory "<<endl;

  //*********************************************************************
  // InjGen(Segments *aInSegments, const string aInjType, const unsigned int aVerbose)
  //*********************************************************************
  cout<<"\nInjGen(Segments *aInSegments, const string aInjType, const unsigned int aVerbose)"<<endl;
  Segments *Seg = new Segments(1126250000, 1126251000);
  Seg->AddSegment(1126253000, 1126254000);
  InjGen *I = new InjGen(Seg, injtype_sinegauss);
  I->SetInjectionSpacing(250);
  cout<<"\tGetN()"<<endl;
  if(I->GetN()!=0) return 2;
  
  //*********************************************************************
  // GenerateInjections(void)
  //*********************************************************************
  cout<<"\nGenerateInjections(void)"<<endl;
  if(I->GenerateInjections()!=8) return 3;
  cout<<"\tGetN()"<<endl;
  if(I->GetN()!=8) return 3;
  delete I;
  delete Seg;

  //*********************************************************************
  // InjTre(const string aInjType, const unsigned int aVerbose=0)
  //*********************************************************************
  cout<<"\nInjTre(const string aInjType, const unsigned int aVerbose=0)"<<endl;
  InjTre *J = new InjTre(injtype_user);
  TGraph *G = new TGraph(4);
  G->SetPoint(0, 0, 0.0);
  G->SetPoint(1, 1, 1.0);
  G->SetPoint(2, 2, 2.0);
  G->SetPoint(3, 3, 3.0);
  cout<<"\tSetWave()"<<endl;
  if(!J->SetWave(G, G)) return 4;
  delete G;
  cout<<"\tAdd()"<<endl;
  if(J->Add(2.0, 2.0, 1126250000.0, 1e-19, 0.2, 1.5, 5.0, 120.0)!=1) return 4;
  if(J->Add(2.0, 2.0, 1126250010.0, 2e-19, 0.3, 1.6, 4.0, 130.0)!=2) return 4;
  if(J->Add(2.0, 2.0, 1126250005.0, 2e-19, 0.3, 1.6, 4.0, 130.0)!=3) return 4;
  if(J->Add(2.0, 2.0, 1126250005.0, 2e-19, 0.3, 1.6, 4.0, 130.0)!=4) return 4;
  if(J->Add(2.0, 2.0, 1126250030.0, 2e-19, 0.3, 1.6, 4.0, 130.0)!=5) return 4;
  if(J->Add(2.0, 2.0, 1126250020.0, 2e-19, 0.3, 1.6, 4.0, 130.0)!=6) return 4;
  if(J->Add(2.0, 2.0, 1126250120.0, 2e-19, 0.3, 1.6, 4.0, 130.0)!=7) return 4;
  if(J->Add(2.0, 2.0, 1126250100.0, 2e-19, 0.3, 1.6, 4.0, 130.0)!=8) return 4;
  cout<<"\tGetN()"<<endl;
  if(J->GetN()!=8) return 4;
  cout<<"\tWrite()"<<endl;
  if(!J->Write("./gwl-test-inject-dir/gw-test-inject1.root")) return 4;
  J->Reset();
  cout<<"\tAdd()"<<endl;
  if(J->Add(2.0, 2.0, 1126250008.0, 1.0e-19, 0.2, 1.5, 5.0, 120.0)!=1) return 4;
  if(J->Add(2.0, 2.0, 1126250065.0, 2.0e-19, 0.31, 1.6, 4.0, 130.0)!=2) return 4;
  if(J->Add(2.0, 2.0, 1126250205.0, 2.0e-19, 0.36, 1.6, 4.0, 130.0)!=3) return 4;
  if(J->Add(2.0, 2.0, 1126250145.0, 2.0e-19, 0.31, 1.6, 4.0, 130.0)!=4) return 4;
  if(J->Add(2.0, 2.0, 1126250130.0, 2.0e-19, 0.37, 1.6, 4.0, 130.0)!=5) return 4;
  if(J->Add(2.0, 2.0, 1126250320.0, 2.0e-19, 0.389, 1.6, 4.0, 130.0)!=6) return 4;
  if(J->Add(2.0, 2.0, 1126250220.0, 2.0e-19, 0.34756, 1.6, 4.0, 130.0)!=7) return 4;
  if(J->Add(2.123, 2.456, 1126250002.765, 3.5e-19, 0.32, 1.123, 0.4, 654.0)!=8) return 4;
  cout<<"\tWrite()"<<endl;
  if(!J->Write("./gwl-test-inject-dir/gw-test-inject2.root")) return 4;
  delete J;

  J = new InjTre(injtype_sinegauss);
  cout<<"\tAdd()"<<endl;
  if(J->Add(2.0, 2.0, 1126260005.0, 1e-19, 0.2, 1.5, 1.0, 120.0)!=1) return 4;
  cout<<"\tGetN()"<<endl;
  if(J->GetN()!=1) return 4;
  cout<<"\tWrite()"<<endl;
  if(!J->Write("./gwl-test-inject-dir/gw-test-inject3.root")) return 4;
  delete J;


  //*********************************************************************
  // InjRea(const string aPattern, const unsigned int aVerbose=0)
  //*********************************************************************
  cout<<"\nInjRea(const string aPattern, const unsigned int aVerbose=0)"<<endl;
  InjRea *R = new InjRea("./gwl-test-inject-dir/gw-test-inject*.root");
  cout<<"\tGetN()"<<endl;
  if(R->GetN()!=17) return 5;
  cout<<"\tGetInjectionType()"<<endl;
  if(R->GetInjectionType()!=injtype_user) return 5;
  cout<<"\tGetInputFilePattern()"<<endl;
  if(R->GetInputFilePattern().compare("./gwl-test-inject-dir/gw-test-inject*.root")!=0) return 5;

  cout<<"\tLoadInjection(1)"<<endl;
  if(R->LoadInjection(1)<=0) return 5;
  cout<<"\tGetInjectionTime()"<<endl;
  if(R->GetInjectionTime()!=1126250002.765) return 5;
  cout<<"\tGetInjectionRa()"<<endl;
  if(R->GetInjectionRa()!=2.123) return 5;
  cout<<"\tGetInjectionDec()"<<endl;
  if(R->GetInjectionDec()!=2.456) return 5;
  cout<<"\tGetInjectionEccentricity()"<<endl;
  if(R->GetInjectionEccentricity()!=0.32) return 5;
  cout<<"\tGetInjectionPolarization()"<<endl;
  if(R->GetInjectionPolarization()!=1.123) return 5;
  cout<<"\tGetInjectionAmplitude()"<<endl;
  if(R->GetInjectionAmplitude()!=3.5e-19) return 5;
  cout<<"\tGetInjectionAmplitudeMin()"<<endl;
  if(R->GetInjectionAmplitudeMin()!=1.0e-19) return 5;
  cout<<"\tGetInjectionAmplitudeMax()"<<endl;
  if(R->GetInjectionAmplitudeMax()!=3.5e-19) return 5;
  cout<<"\tGetInjectionSigma()"<<endl;
  if(R->GetInjectionSigma()!=0.4) return 5;
  cout<<"\tGetInjectionSigmaMin()"<<endl;
  if(R->GetInjectionSigmaMin()!=0.4) return 5;
  cout<<"\tGetInjectionSigmaMax()"<<endl;
  if(R->GetInjectionSigmaMax()!=5.0) return 5;
  cout<<"\tGetInjectionFrequency()"<<endl;
  if(R->GetInjectionFrequency()!=654.0) return 5;
  cout<<"\tGetInjectionFrequencyMin()"<<endl;
  if(R->GetInjectionFrequencyMin()!=120.0) return 5;
  cout<<"\tGetInjectionFrequencyMax()"<<endl;
  if(R->GetInjectionFrequencyMax()!=654.0) return 5;

  cout<<"\tLoadInjection(0)"<<endl;
  if(R->LoadInjection(0)<=0) return 5;
  cout<<"\tGetInjectionTime()"<<endl;
  if(R->GetInjectionTime()!=1126250000.0) return 5;
  cout<<"\tLoadInjection(4)"<<endl;
  if(R->LoadInjection(4)<=0) return 5;
  cout<<"\tGetInjectionTime()"<<endl;
  if(R->GetInjectionTime()!=1126250008.0) return 5;
  cout<<"\tLoadInjection(8)"<<endl;
  if(R->LoadInjection(8)<=0) return 5;
  cout<<"\tGetInjectionTime()"<<endl;
  if(R->GetInjectionTime()!=1126250065.0) return 5;
  cout<<"\tLoadInjection(12)"<<endl;
  if(R->LoadInjection(12)<=0) return 5;
  cout<<"\tGetInjectionTime()"<<endl;
  if(R->GetInjectionTime()!=1126250145.0) return 5;
  cout<<"\tLoadInjection(15)"<<endl;
  if(R->LoadInjection(15)<=0) return 5;
  cout<<"\tGetInjectionTime()"<<endl;
  if(R->GetInjectionTime()!=1126250320.0) return 5;

  if(R->LoadInjection(10)<=0) return 5;
  R->SetInjectionTag(false);
  cout<<"\tSetInjectionTag()"<<endl;
  if(R->LoadInjection(7)<=0) return 5;
  cout<<"\tGetInjectionTag()"<<endl;
  if(R->GetInjectionTag()==false) return 5;
  if(R->LoadInjection(10)<=0) return 5;
  cout<<"\tGetInjectionTag()"<<endl;
  if(R->GetInjectionTag()==true) return 5;

  cout<<"\tLoadInjection(0)"<<endl;
  if(R->LoadInjection(0)<=0) return 5;
  cout<<"\tGetInjectionTimeStart()"<<endl;
  if(R->GetInjectionTimeStart()!=1126250000.0) return 5;
  cout<<"\tGetInjectionTimeEnd()"<<endl;
  if(R->GetInjectionTimeEnd()!=1126250003.0) return 5;
  cout<<"\tLoadInjection(12)"<<endl;
  if(R->LoadInjection(12)<=0) return 5;
  cout<<"\tGetInjectionTimeStart()"<<endl;
  if(R->GetInjectionTimeStart()!=1126250145.0) return 5;
  cout<<"\tGetInjectionTimeEnd()"<<endl;
  if(R->GetInjectionTimeEnd()!=1126250148.0) return 5;

  cout<<"\tLoadInjection(16)"<<endl;
  if(R->LoadInjection(16)<=0) return 5;
  cout<<"\tGetInjectionTimeStart()"<<endl;
  if(R->GetInjectionTimeStart()!=R->GetInjectionTime()-10.0*R->GetInjectionSigma()) return 5;
  cout<<"\tGetInjectionTimeEnd()"<<endl;
  if(R->GetInjectionTimeEnd()!=R->GetInjectionTime()+10.0*R->GetInjectionSigma()) return 5;
 
  delete R;

  //*********************************************************************
  // InjEct(Streams* aStream, const string aPattern, const string aPlotStyle="STANDARD", const unsigned int aVerbose=0)
  //*********************************************************************
  cout<<"\nInjEct(Streams* aStream, const string aPattern, const string aPlotStyle, const unsigned int aVerbose)"<<endl;
  Streams *St = new Streams("H1:TOTO");
  St->SetFrequencies(1024, 1024);
  InjEct *IE = new InjEct(St, "./gwl-test-inject-dir/gw-test-inject*.root");
  cout<<"\tGetN()"<<endl;
  if(IE->GetN()!=17) return 6;

  cout<<"\tInject()"<<endl;
  unsigned int datasize = 10*St->GetNativeFrequency();
  double *data = new double [datasize];
  for(unsigned int i=0; i<datasize; i++) data[i] = 0.0;
  IE->Inject(datasize, data, 1126260000.0);//sine-Gauss

  for(unsigned int i=0; i<datasize; i++) data[i] = 0.0;
  IE->Inject(datasize, data, 1126250200);// user wave


  delete data;
  delete St;
  delete IE;
  
  if(!filesystem::remove_all("./gwl-test-inject-dir")) cerr<<"clean-up failed"<<endl;

  return 0;
}

