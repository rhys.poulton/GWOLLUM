//////////////////////////////////////////////////////////////////////////////
//  Author : florent robinet (LAL - Orsay): robinet@lal.in2p3.fr
//////////////////////////////////////////////////////////////////////////////
#ifndef __InjCoi__
#define __InjCoi__

#include "MorphMatch.h"
#include "InjRec.h"

using namespace std;

/**
 * Recover injections in a coinc set.
 * \author    Florent Robinet
 */
class InjCoi: public MorphMatch {
  
 public:
  
  /**
   * @name Constructors and destructors
   @{
  */
  /**
   * Constructor of the InjCoi class.
   * Two injection recovery streams (see InjRec class) will be put in coincidence. See the Coinc2 parent class to perform the coincidence.
   *
   * In this constructor, the set of valid injections is selected. It is defined as the intersection of both valid injection sets. Of course, for both recovery streams, the injection set must be the same!
   *
   * NOTE: Unless you know what you're doing, it is better not to modify the input injection recovery streams while using the methods of this class.
   * @param aRec0 first input injection recovery stream
   * @param aRec1 second input injection recovery stream
   * @param aVerbose verbosity level
   */
  InjCoi(InjRec *aRec0, InjRec *aRec1, const int aVerbose=0);

  /**
   * Destructor of the InjCoi class.
   */
  virtual ~InjCoi(void);
  /**
     @}
  */
    
  /**
   * Recovers injections in the coinc set.
   * An injection is found if it is recovered in at least one trigger stream. See InjRec::Recover().
   * Returns the number of found injections. Returns -1 if the function fails in some way.
   * If an injection index is given in argument, information is printed to track the injection through the recovering process.
   *
   * NOTE1: input clusters for both streams are specifically tagged to map coincs. A discarded cluster is tagged with -1 and a valid cluster is tagged with 1 (see Coinc2::SetTriggerSample()). In this function, clusters involved in a coinc event are tagged with 2.
   *
   * NOTE2: Coinc matching an injection are tagged to true (false otherwise). See Coinc2::SetCoincTag().
   * @param aInjIndex injection index to follow-up
   */
  int Recover(const int aInjIndex=-1);

  /**
   * Writes coinc events on disk.
   * This function overloads the parent function MorphMatch::WriteCoinc(). Two additional trees are saved. An injection tree is saved with only valid injections. A branch called 'injindex' is used to map this set of valid injections with the original injection tree (entry number). A tree called 'coincmap' connects the injection tree with coinc events. The injection index is reported for each coinc.
   * Only coinc with a tag set to true are considered (i.e. matching a valid injection), see Coinc2::SetCoincTag().
   * @param aOutDir output directory (must exist)
   * @param aOutFileName output file name (do not provide an extension)
   */
  bool WriteCoinc(const string aOutDir, const string aOutFileName);

 private:

  int Verbose;
  bool status_OK;

  vector <int> rec_injindex; ///< recovered injection index
  vector <int> coinc_injindex;///< injection index matching coinc

  InjRec *rec0;              ///< first input trigger set
  InjRec *rec1;              ///< second input trigger set

  ClassDef(InjCoi,0)
};

#endif


