/**
 * @file 
 * @brief See InjEct.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "InjEct.h"

ClassImp(InjEct)

/////////////////////////////////////////////////////////////////////////
InjEct::InjEct(Streams* aStream, const string aPattern,
               const string aPlotStyle, const unsigned int aVerbose):
InjRea(aPattern, aVerbose), GwollumPlot("INJ", aPlotStyle){ 
/////////////////////////////////////////////////////////////////////////

  // for plotting
  SetGridx();  SetGridy();
  
  InStream = aStream;
  
  // dummy containers
  hplus  = new TGraph(0); hplus->SetName("InjEct_hplus");
  hcross = new TGraph(0); hcross->SetName("InjEct_hcross");
  hdet   = new TGraph(0); hdet->SetName("InjEct_hdet");
}

/////////////////////////////////////////////////////////////////////////
InjEct::~InjEct(void){
/////////////////////////////////////////////////////////////////////////
  if(Verbose>1) cout<<"InjEct::~InjEct"<<endl;
  delete hplus;
  delete hcross;
  delete hdet;
}

/////////////////////////////////////////////////////////////////////////
void InjEct::Inject(const unsigned int aDataSize, double *aData, const double aTimeStart){
/////////////////////////////////////////////////////////////////////////


  // segment start - end
  double sstart = aTimeStart;
  double send = sstart + (double)aDataSize/(double)InStream->GetNativeFrequency();

  double istart_geocentric, iend_geocentric, istart_local, iend_local;
  double t_local;
  
  // loop over injections
  for(Long64_t i=0; i<InjRea::GetN(); i++){

    // load injection
    if(LoadInjection(i)<=0) continue;

    // injection time at the center of the Earth
    istart_geocentric = InjRea::GetInjectionTimeStart();
    iend_geocentric = InjRea::GetInjectionTimeEnd();

    // local injection time
    istart_local = InStream->GetLocalTime(InjRea::GetInjectionRa(), InjRea::GetInjectionDec(), istart_geocentric);
    iend_local = InStream->GetLocalTime(InjRea::GetInjectionRa(), InjRea::GetInjectionDec(), iend_geocentric);
    
    // the injection ends before the requested segment
    if(iend_local<sstart) continue;

    // the injection starts after the requested segment
    if(istart_local>=send) continue;

    // construct the local waveform
    if(InjRea::GetInjectionType()==injtype_user) ConstructUserWaveform();
    else if(InjRea::GetInjectionType()==injtype_sinegauss) ConstructSineGaussWaveform();
    else continue;

    // add injection waveform
    for(unsigned int d=0; d<aDataSize; d++){

      // local time
      t_local = aTimeStart+(double)d/(double)InStream->GetNativeFrequency();

      // out of range
      if(t_local<hdet->GetX()[0]) continue;
      if(t_local>=hdet->GetX()[hdet->GetN()-1]) break;

      // inject
      aData[d] += hdet->Eval(aTimeStart+(double)d/(double)InStream->GetNativeFrequency());
    }

  }

  return;  
}

/////////////////////////////////////////////////////////////////////////
void InjEct::Plot(const Long64_t aInjectionIndex){
/////////////////////////////////////////////////////////////////////////

  // load injection
  if(LoadInjection(aInjectionIndex)<=0) return;
  
  // construct the local waveform
  if(InjRea::GetInjectionType()==injtype_user) ConstructUserWaveform();
  else if(InjRea::GetInjectionType()==injtype_sinegauss) ConstructSineGaussWaveform();
  else return;
  
  
  GwollumPlot::Draw(hplus, "APL");
  GwollumPlot::Draw(hcross, "PLsame");
  GwollumPlot::Draw(hdet, "PLsame");
  
  return;
}

/////////////////////////////////////////////////////////////////////////
void InjEct::ConstructUserWaveform(void){
/////////////////////////////////////////////////////////////////////////
  
  // reset h+ (if size change)
  if(hplus->GetN() != InjRea::wave_hplus->GetN()){
    delete hplus;
    hplus = new TGraph(InjRea::wave_hplus->GetN()); hplus->SetName("InjEct_hplus");
    hplus->GetXaxis()->SetNoExponent();
    hplus->SetTitle("h_{+}");
    hplus->SetBit(TGraph::kIsSortedX);
  }

  // reset hx (if size change)
  if(hcross->GetN() != InjRea::wave_hcross->GetN()){
    delete hcross;
    hcross = new TGraph(InjRea::wave_hcross->GetN()); hcross->SetName("InjEct_hcross");
    hcross->GetXaxis()->SetNoExponent();
    hcross->SetTitle("h_{x}");
    hcross->SetBit(TGraph::kIsSortedX);
  }
  
  // reset hdet (if size change)
  if(hdet->GetN() != InjRea::wave_hplus->GetN()){
    delete hdet;
    hdet = new TGraph(InjRea::wave_hplus->GetN()); hdet->SetName("InjEct_hdet");
    hdet->GetXaxis()->SetNoExponent();
    hdet->SetTitle("h_{det}");
    hdet->SetBit(TGraph::kIsSortedX);
  }
  
  // tukey window
  double *tukey = GetTukeyWindow((unsigned int)InjRea::wave_hplus->GetN(), 0.1);
  
  double t_local, amplitude;
  double fplus, fcross;
  
  // waveforms at the detector
  for(unsigned int i=0; i<(unsigned int)InjRea::wave_hplus->GetN(); i++){
    
    // local time
    t_local = InStream->GetLocalTime(InjRea::GetInjectionRa(), InjRea::GetInjectionDec(), InjRea::wave_hplus->GetX()[i]+GetInjectionTime());
    
    // local h+
    amplitude = InjRea::GetInjectionAmplitude()*InjRea::wave_hplus->GetY()[i];
    hplus->SetPoint(i, t_local, amplitude);
    
    // local hx
    amplitude = InjRea::GetInjectionAmplitude()*InjRea::wave_hcross->GetY()[i];
    hcross->SetPoint(i, t_local, amplitude);
    
    // antenna factors
    InStream->GetDetectorAMResponse(fplus, fcross,
                                    InjRea::GetInjectionRa(), InjRea::GetInjectionDec(), InjRea::GetInjectionPolarization(),
                                    GreenwichMeanSiderealTime(hplus->GetX()[i]));
    
    // hdet
    hdet->SetPoint(i, t_local, tukey[i]*(fplus * hplus->GetY()[i] + fcross * hcross->GetY()[i]));
  }
 
  delete tukey;
  return;
}

/////////////////////////////////////////////////////////////////////////
void InjEct::ConstructSineGaussWaveform(void){
/////////////////////////////////////////////////////////////////////////
  
  // number of points
  unsigned int npoints = (InjRea::GetInjectionTimeEnd()-InjRea::GetInjectionTimeStart())*(double)InStream->GetNativeFrequency();

  // reset h+ (if size change)
  if((unsigned int)hplus->GetN() != npoints){
    delete hplus;
    hplus = new TGraph(npoints); hplus->SetName("InjEct_hplus");
    hplus->GetXaxis()->SetNoExponent();
    hplus->SetTitle("h_{+}");
    hplus->SetBit(TGraph::kIsSortedX);
  }
  
  // reset hx (if size change)
  if((unsigned int)hcross->GetN() != npoints){
    delete hcross;
    hcross = new TGraph(npoints); hcross->SetName("InjEct_hcross");
    hcross->GetXaxis()->SetNoExponent();
    hcross->SetTitle("h_{x}");
    hcross->SetBit(TGraph::kIsSortedX);
  }
  
  // reset hdet (if size change)
  if((unsigned int)hdet->GetN() != npoints){
    delete hdet;
    hdet = new TGraph(npoints); hdet->SetName("InjEct_hdet");
    hdet->GetXaxis()->SetNoExponent();
    hdet->SetTitle("h_{det}");
    hdet->SetBit(TGraph::kIsSortedX);
  }
  
  // tukey window
  double *tukey = GetTukeyWindow(npoints, 0.1);

  double t_local, t_geocentric;
  double fplus, fcross;
  
  // "peak" amplitudes of h+ and hx
  double h0plus  = InjRea::GetSineGaussh0plus();
  double h0cross = InjRea::GetSineGaussh0cross();
    
  // parameters
  double sigma  = 2.0 * InjRea::GetInjectionSigma()*InjRea::GetInjectionSigma();
  double factor = 2.0 * TMath::Pi() * InjRea::GetInjectionFrequency();
    
  // waveforms at the detector
  for(unsigned int i=0; i<(unsigned int)hplus->GetN(); i++){

    // local time
    t_geocentric = ((double)i-(double)hplus->GetN()/2.0)/(double)InStream->GetNativeFrequency();
    t_local = InStream->GetLocalTime(InjRea::GetInjectionRa(), InjRea::GetInjectionDec(), t_geocentric+InjRea::GetInjectionTime());
      
    // local h+
    hplus->SetPoint(i, t_local, h0plus * exp(-t_geocentric*t_geocentric/sigma) * cos(factor*t_geocentric));

    // local hx at the detector
    hcross->SetPoint(i, t_local, h0cross * exp(-t_geocentric*t_geocentric/sigma) * sin(factor*t_geocentric));

    // antenna factors
    InStream->GetDetectorAMResponse(fplus, fcross,
                                    InjRea::GetInjectionRa(), InjRea::GetInjectionDec(), InjRea::GetInjectionPolarization(),
                                    GreenwichMeanSiderealTime(hplus->GetX()[i]));
    
    // hdet
    hdet->SetPoint(i, t_local, tukey[i]*(fplus*hplus->GetY()[i] + fcross*hcross->GetY()[i]));
  }

  delete tukey;
  return;
}

