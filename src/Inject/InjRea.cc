/**
 * @file 
 * @brief See InjRea.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "InjRea.h"

ClassImp(InjRea)

/////////////////////////////////////////////////////////////////////////
InjRea::InjRea(const string aPattern, const unsigned int aVerbose){ 
/////////////////////////////////////////////////////////////////////////

  // parameters
  Verbose=aVerbose;
  pattern=aPattern;
  
  // get list of patterns and load it
  vector <string> patlist = SplitString(pattern,' ');

  // injection tree
  InjTree = new TChain("Injections");
  for(unsigned int p=0; p<patlist.size(); p++) InjTree->Add(patlist[p].c_str());
  InjTree->SetDirectory(0);

  // TTree index
  sort_index = new TTreeIndex(InjTree, "time","0");
  inj_index = -1;

  // attach branches
  if(InjTree->SetBranchAddress("ra", &inj_ra)<0) inj_ra=-1.0;
  if(InjTree->SetBranchAddress("dec", &inj_dec)<0) inj_dec=-1.0;
  if(InjTree->SetBranchAddress("time", &inj_time)<0) inj_time=-1.0;
  if(InjTree->SetBranchAddress("eccentricity", &inj_ecc)<0) inj_ecc=-1.0;
  if(InjTree->SetBranchAddress("polarization", &inj_psi)<0) inj_psi=-1.0;
  if(InjTree->SetBranchAddress("amplitude", &inj_amp)<0){
    inj_amp = -1.0;
    inj_ampmin = -1.0;
    inj_ampmax = -1.0;
  }
  else{
    inj_ampmin = InjTree->GetMinimum("amplitude");
    inj_ampmax = InjTree->GetMaximum("amplitude");
  }
  if(InjTree->SetBranchAddress("sigma", &inj_sigma)<0){
    inj_sigma = -1.0;
    inj_sigmamin = -1.0;
    inj_sigmamax = -1.0;
  }
  else{
    inj_sigmamin = InjTree->GetMinimum("sigma");
    inj_sigmamax = InjTree->GetMaximum("sigma");
  }
  if(InjTree->SetBranchAddress("frequency", &inj_f0)<0){
    inj_f0=-1.0;
    inj_f0min=-1.0;
    inj_f0max=-1.0;
  }
  else{
    inj_f0min=InjTree->GetMinimum("frequency");
    inj_f0max=InjTree->GetMaximum("frequency");
  }

  // waveform tree
  wave_hplus = 0;
  wave_hcross = 0;
  WaveTree = new TChain("Waveform");
  for(unsigned int p=0; p<patlist.size(); p++) WaveTree->Add(patlist[p].c_str());
  patlist.clear();
  WaveTree->SetDirectory(0);

  if(WaveTree->SetBranchAddress("wavetype", &wave_type)<0) wave_type=(unsigned int)injtype_n;
  if(WaveTree->SetBranchAddress("hplus", &wave_hplus)<0){
    wave_hplus = new TGraph(64);
    for(unsigned int i=0; i<(unsigned int)wave_hplus->GetN(); i++) wave_hplus->SetPoint(i, (double)i/(double)wave_hplus->GetN(), 0.0);
  }
  if(WaveTree->SetBranchAddress("hcross", &wave_hcross)<0){
    wave_hcross = new TGraph(64);
    for(unsigned int i=0; i<(unsigned int)wave_hcross->GetN(); i++) wave_hcross->SetPoint(i, (double)i/(double)wave_hcross->GetN(), 0.0);
  }

  // tags
  inj_tag = new bool [InjTree->GetEntries()];
  for(Long64_t i=0; i<InjTree->GetEntries(); i++) inj_tag[i]=true;

  // set to first injection
  LoadInjection(0);

  if(Verbose){
    cout<<"InjRea::InjRea: "<<InjTree->GetEntries()<<" injections are loaded"<<endl;
    if(Verbose>1){
      cout<<"                frequency: "<<inj_f0min<<" --> "<<inj_f0max<<" Hz"<<endl;
      cout<<"                sigma:     "<<inj_sigmamin<<" --> "<<inj_sigmamax<<" s"<<endl;
      cout<<"                amplitude: "<<scientific<<inj_ampmin<<" --> "<<inj_ampmax<<endl;
    }
  }
}

/////////////////////////////////////////////////////////////////////////
InjRea::~InjRea(void){
/////////////////////////////////////////////////////////////////////////
  if(Verbose>1) cout<<"InjRea::~InjRea"<<endl;
  delete InjTree;
  //delete sort_index;
  delete inj_tag;
  delete wave_hplus;
  delete wave_hcross;
}

/////////////////////////////////////////////////////////////////////////
int InjRea::LoadInjection(const Long64_t aInjIndex){
/////////////////////////////////////////////////////////////////////////
  if((aInjIndex<0)||(aInjIndex>=InjTree->GetEntries())) return 0;

  // load injection
  int nbytes_inj = InjTree->GetEntry(sort_index->GetIndex()[aInjIndex]);

  // current injection index
  if(nbytes_inj>0) inj_index = aInjIndex;
  else return 0;

  // load waveform
  int nbytes_wav = WaveTree->GetEntry(InjTree->GetTreeNumber());
  if(nbytes_inj==0) return 0;
  return nbytes_inj + nbytes_wav;
}

/////////////////////////////////////////////////////////////////////////
double InjRea::GetSineGaussh0plus(void){
/////////////////////////////////////////////////////////////////////////
  // !!! here, amplitude = hrss !!!
  double q = 2 * TMath::Pi() * inj_sigma * inj_f0;
  
  // semimajor and semiminor axes of waveform ellipsoid
  const double a = 1.0 / sqrt(2.0 - inj_ecc * inj_ecc);
  const double b = a * sqrt(1.0 - inj_ecc * inj_ecc);

  // rss of plus polarization
  const double hplusrss  = inj_amp * (a * cos(inj_psi) - b * sin(inj_psi));
  
  // rss of unit amplitude cosine-gaussian waveforms.  see
  // K. Riles, LIGO-T040055-00.pdf
  const double cgrss = sqrt((q / (4.0 * inj_f0 * sqrt(TMath::Pi()))) * (1.0 + exp(-q * q)));
  
  // "peak" amplitude
  return hplusrss / cgrss;
}

/////////////////////////////////////////////////////////////////////////
double InjRea::GetSineGaussh0cross(void){
/////////////////////////////////////////////////////////////////////////
  // !!! here, amplitude = hrss !!!
  double q = 2 * TMath::Pi() * inj_sigma * inj_f0;

  // semimajor and semiminor axes of waveform ellipsoid
  const double a = 1.0 / sqrt(2.0 - inj_ecc * inj_ecc);
  const double b = a * sqrt(1.0 - inj_ecc * inj_ecc);
  
  // rss of cross polarizations
  const double hcrossrss = inj_amp * (b * cos(inj_psi) + a * sin(inj_psi));
  
  // rss of unit amplitude sine-gaussian waveforms.  see
  // K. Riles, LIGO-T040055-00.pdf
  const double sgrss = sqrt((q / (4.0 * inj_f0 * sqrt(TMath::Pi()))) * (1.0 - exp(-q * q)));

  // "peak" amplitude
  return hcrossrss / sgrss;
}

/////////////////////////////////////////////////////////////////////////
double InjRea::GetInjectionTimeStart(void){
/////////////////////////////////////////////////////////////////////////

  // user waveform --> start of h+
  if(GetInjectionType()==injtype_user){
    return wave_hplus->GetX()[0]+inj_time;
  }
  // Sine-Gaussian waveform
  else if(GetInjectionType()==injtype_sinegauss){
    // FIXME: 10 sigma???
    return inj_time-10.0*inj_sigma;
  }
  else{};

  return 0.0;
}

/////////////////////////////////////////////////////////////////////////
double InjRea::GetInjectionTimeEnd(void){
/////////////////////////////////////////////////////////////////////////
  
  // user waveform --> end of h+
  if(GetInjectionType()==injtype_user){
    return wave_hplus->GetX()[wave_hplus->GetN()-1]+inj_time;
  }
  // Sine-Gaussian waveform
  else if(GetInjectionType()==injtype_sinegauss){
    // FIXME: 10 sigma???
    return inj_time+10.0*inj_sigma;
  }
  else{};

  return 0.0;
}

/////////////////////////////////////////////////////////////////////////
TH1D* InjRea::GetInjectionParamDist(const string aParamName,
                                    const unsigned int aNbins, const string aBinType){
/////////////////////////////////////////////////////////////////////////
  if(!InjTree->GetEntries()) return NULL;
  TH1D *h = NULL;

  // linear bins
  if(!aBinType.compare("UNIFORM")){

    // amplitude
    if(!aParamName.compare("amplitude"))
      h = new TH1D("h_inj_amp","Injection amplitude",
                   aNbins, inj_ampmin, inj_ampmax*1.01);

    // f0
    if(!aParamName.compare("f0"))
      h = new TH1D("h_inj_f0","Injection frequency",
                   aNbins, inj_f0min, inj_f0max*1.01);
  }

  // log bins
  if(!aBinType.compare("LOG")){
    double *bins = new double [aNbins+1];

    // amplitude
    if(!aParamName.compare("amplitude")){
      for(unsigned int b=0; b<aNbins+1; b++) 
	bins[b] = inj_ampmin*pow(10.0,b*log10(2*inj_ampmax/inj_ampmin)/(double)aNbins);
      h = new TH1D("h_inj_amp","Injection amplitude",aNbins,bins);
    }

    // f0
    if(!aParamName.compare("f0")){
      for(unsigned int b=0; b<aNbins+1; b++) 
	bins[b] = inj_f0min*pow(10.0,b*log10(2*inj_f0max/inj_f0min)/(double)aNbins);
      h = new TH1D("h_inj_f0","Injection frequency",aNbins,bins);
    }

    delete bins;
  }

  // fill histo
  if(h!=NULL){
    h->GetYaxis()->SetTitle("Number of injections / bin");

    // amplitude
    if(!aParamName.compare("amplitude")){
      h->GetXaxis()->SetTitle("Injection amplitude");
      for(Long64_t i=0; i<GetN(); i++){
	LoadInjection(i);
	if(GetInjectionTag()) h->Fill(GetInjectionAmplitude());
      }
    }

    // f0
    if(!aParamName.compare("f0")){
      h->GetXaxis()->SetTitle("Injection frequency [Hz]");
      for(Long64_t i=0; i<GetN(); i++){
	LoadInjection(i);
	if(GetInjectionTag()) h->Fill(GetInjectionFrequency());
      }
    }
  } 
  
  return h;
}

