//////////////////////////////////////////////////////////////////////////////
//  Author : florent robinet (LAL - Orsay): robinet@lal.in2p3.fr
//////////////////////////////////////////////////////////////////////////////
#include "InjCoi.h"

ClassImp(InjCoi)


/////////////////////////////////////////////////////////////////////////
InjCoi::InjCoi(InjRec *aRec0, InjRec *aRec1, const int aVerbose): MorphMatch(aRec0,aRec1,aVerbose){ 
/////////////////////////////////////////////////////////////////////////

  // input parameters
  Verbose=aVerbose;
  rec0 = aRec0;
  rec1 = aRec1;
  status_OK=true;
  
  //check that the 2 injection sets are the same
  if(rec0->GetNInjections()!=rec1->GetNInjections()){
    cerr<<"InjCoi::InjCoi: the injection sets for the 2 input trigger streams aren't the same"<<endl;
    status_OK=false;
  }
  rec0->LoadInjection(0); rec1->LoadInjection(0);
  if(rec0->GetInjectionRa()!=rec1->GetInjectionRa()){// random check
    cerr<<"InjCoi::InjCoi: the injection sets for the 2 input trigger streams aren't the same"<<endl;
    status_OK=false;
  }

  // intersect valid injection sets
  int n=0;
  for(int i=0; i<rec0->GetNInjections(); i++){
    rec0->LoadInjection(i);
    rec1->LoadInjection(i);
    if(!rec0->GetInjectionTag()) rec1->SetInjectionTag(false);
    if(!rec1->GetInjectionTag()) rec0->SetInjectionTag(false);
    if(rec0->GetInjectionTag()) n++;
  }
  if(Verbose>1) cout<<"InjCoi::InjCoi: number of valid injections = "<<n<<" for "<<rec0->GetNamePrefix()<<rec1->GetNamePrefix()<<" coinc stream"<<endl;

}

/////////////////////////////////////////////////////////////////////////
InjCoi::~InjCoi(void){
/////////////////////////////////////////////////////////////////////////
  if(Verbose>1) cout<<"InjCoi::~InjCoi"<<endl;
  rec_injindex.clear();
  coinc_injindex.clear();
}

/////////////////////////////////////////////////////////////////////////
int InjCoi::Recover(const int aInjIndex){
/////////////////////////////////////////////////////////////////////////

  // reset
  coinc_injindex.clear();
  rec_injindex.clear();

  // only activate clusters involved in a valid coinc (for InjRec::Recover())
  for(int c=0; c<(int)CoC[0].size(); c++){
    rec0->SetClusterTag(CoC[0][c],2);
    rec1->SetClusterTag(CoC[1][c],2);
  }
  
  // recover injections in each stream
  if(Verbose) cout<<"InjCoi::Recover: recover injections in each trigger set..."<<endl;
  rec0->Recover(aInjIndex,1);
  rec1->Recover(aInjIndex,1);
  
  // get the list of recovered injections: union of recovered injections in each stream
  if(Verbose) cout<<"InjCoi::Recover: combine recovery streams..."<<endl;
  rec_injindex = rec0->GetRecInjections();
  vector <int> tmp = rec1->GetRecInjections();
  rec_injindex.insert(rec_injindex.end(), tmp.begin(), tmp.end());
  tmp.clear();
  sort( rec_injindex.begin(), rec_injindex.end() );
  rec_injindex.erase( unique( rec_injindex.begin(), rec_injindex.end() ), rec_injindex.end() );
 
  // match coincs to an injection
  // should be larger than rec_injindex.size()
  SetCoincTag(false);
  int r0start=0, r1start=0, n=0;
  for(int c=0; c<(int)CoC[0].size(); c++){
    for(int r0=r0start; r0<rec0->GetNRecInjections(); r0++){// check with first stream
      r0start=r0;
      if(rec0->rec_clindex[r0]==CoC[0][c]){
	SetCoincTag(c,true);
	coinc_injindex.push_back(rec0->rec_injindex[r0]);
	n++;
	break;
      }
      if(rec0->rec_clindex[r0]>CoC[0][c]) break;
    }
    if(GetCoincTag(c)) continue;
    for(int r1=r1start; r1<rec1->GetNRecInjections(); r1++){// check with second stream
      r1start=r1;
      if(rec1->rec_clindex[r1]==CoC[1][c]){
	SetCoincTag(c,true);
	coinc_injindex.push_back(rec1->rec_injindex[r1]);
	n++;
	break;
      }
      if(rec1->rec_clindex[r1]>CoC[1][c]) break;
    }
  }
 
  if(Verbose>1){
    cout<<"InjCoi::Recover: "<<rec_injindex.size()<<" injections are recovered in the coinc events"<<endl;
    cout<<"                 "<<n<<" coinc events were found matching an injection"<<endl;
  }
  return (int)rec_injindex.size();
}

////////////////////////////////////////////////////////////////////////////////////
bool InjCoi::WriteCoinc(const string aOutDir, const string aOutFileName){
////////////////////////////////////////////////////////////////////////////////////

  // save coinc first
  if(!MorphMatch::WriteCoinc(aOutDir,aOutFileName)) return false;

  // coinc <-> injections matching
  TTree *coincmap = new TTree("coincmap","coincmap");
  int cmap;
  coincmap->Branch("injindex", &cmap, "injindex/I");// valid injection re-indexing!

  // assign the valid injection index to each coinc
  if(verbosity) cout<<"InjCoi::WriteCoinc: fill coincmap tree..."<<endl;
  int rstart=0;
  rec0->ValidInjTree->SetBranchStatus("*",0);
  rec0->ValidInjTree->SetBranchStatus("injindex",1);
  for(int c=0; c<(int)coinc_injindex.size(); c++){
    cmap=-1;
    for(int r=rstart; r<rec0->ValidInjTree->GetEntries(); r++){
      rec0->ValidInjTree->GetEntry(r);
      rstart=r;
      if(coinc_injindex[c]==rec0->injindex){
	cmap=r;
	break;
      }
    }
    if(cmap<0) return false;
    else coincmap->Fill();
  }
  rec0->ValidInjTree->SetBranchStatus("*",1);
  rec0->LoadInjection(0);// to get back on track

  // save trees
  if(verbosity) cout<<"InjCoi::WriteCoinc: save injection and coincmap trees..."<<endl;
  TFile fout((aOutDir+"/"+aOutFileName+".root").c_str(),"UPDATE");
  if(fout.IsZombie()){ delete coincmap; return false; }
  fout.cd();
  rec0->ValidInjTree->Write();
  coincmap->Write();
  fout.Close();

  delete coincmap;

  return true;
}
