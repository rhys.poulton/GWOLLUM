/**
 * @file 
 * @brief See InjTre.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "InjTre.h"

ClassImp(InjTre)


/////////////////////////////////////////////////////////////////////////
InjTre::InjTre(const injtype aInjType, const unsigned int aVerbose){ 
/////////////////////////////////////////////////////////////////////////

  Verbose=aVerbose;

  // random id
  srand(time(NULL));
  randid = rand();
  gwl_ss<<randid;
  srandid = gwl_ss.str();
  gwl_ss.clear(); gwl_ss.str("");

  // waveform type
  wave_type = (UInt_t)aInjType;
  
  // default values
  inj_ra = 0.0;
  inj_dec = 0.0;
  inj_time = 0.0;
  inj_ecc = 0.0;
  inj_psi = 0.0;
  inj_amp = 0.0;
  inj_sigma = 0.0;
  inj_f0 = 0.0;

  // default waveforms
  wave_hplus = new TGraph(64);
  wave_hplus->SetName("gw_inj_hplus");
  wave_hcross = new TGraph(64);
  wave_hcross->SetName("gw_inj_hcross");
  for(unsigned int i=0; i<(unsigned int)wave_hplus->GetN(); i++){
    wave_hplus->SetPoint(i, (double)i/(double)wave_hplus->GetN(), 0.0);
    wave_hcross->SetPoint(i, (double)i/(double)wave_hplus->GetN(), 0.0);
  }
  
  // injection TTree
  InjTree = new TTree(("Injections_"+srandid).c_str(), "Injections");
  InjTree->Branch("ra",           &inj_ra,    "ra/D");
  InjTree->Branch("dec",          &inj_dec,   "dec/D");
  InjTree->Branch("time",         &inj_time,  "time/D");
  InjTree->Branch("eccentricity", &inj_ecc,   "eccentricity/D");
  InjTree->Branch("polarization", &inj_psi,   "polarization/D");
  InjTree->Branch("amplitude",    &inj_amp,   "amplitude/D");
  InjTree->Branch("sigma",        &inj_sigma, "sigma/D");
  InjTree->Branch("frequency",    &inj_f0,    "frequency/D");

  // waveform TTree
  WaveTree = new TTree(("Waveform_"+srandid).c_str(), "Waveform");
  WaveTree->Branch("wavetype", &wave_type, "wavetype/i");
  WaveTree->Branch("hplus", "TGraph", &wave_hplus);
  WaveTree->Branch("hcross", "TGraph", &wave_hcross);

}

/////////////////////////////////////////////////////////////////////////
InjTre::~InjTre(void){
/////////////////////////////////////////////////////////////////////////
  delete InjTree;
  delete WaveTree;
  delete wave_hplus;
  delete wave_hcross;
}

/////////////////////////////////////////////////////////////////////////
bool InjTre::SetWave(TGraph *aHplus, TGraph *aHcross){
/////////////////////////////////////////////////////////////////////////

  // h+ must exist
  if(aHplus==NULL) return false;
  if(aHplus==0) return false;

  // h+ must have at least one point
  if(aHplus->GetN()==0) return false;

  // h+ and hx must have the same number of points
  if((aHcross!=NULL)&&(aHcross->GetN()!=aHplus->GetN())) return false;

  // h+ and hx must be aligned in time
  if((aHcross!=NULL)&&(aHcross->GetX()[0]!=aHplus->GetX()[0])) return false;

  // reset
  WaveTree->Reset();
  delete wave_hplus;
  delete wave_hcross;

  // save parameters
  wave_hplus = (TGraph*)aHplus->Clone("gw_inj_hplus");
  if(aHcross!=NULL){
    wave_hcross = (TGraph*)aHcross->Clone("gw_inj_hcross");
  }
  else{
    wave_hcross = new TGraph(wave_hplus->GetN());
    wave_hcross->SetName("gw_inj_hcross");
    for(unsigned int i=0; i<(unsigned int)wave_hplus->GetN(); i++){
      wave_hcross->SetPoint(i, wave_hplus->GetX()[i], 0.0);
    }
  }
  
  return true;
}

/////////////////////////////////////////////////////////////////////////
unsigned int InjTre::Add(const double aRa, 
                         const double aDec, 
                         const double aTime, 
                         const double aAmplitude, 
                         const double aEccentricity, 
                         const double aPolarization, 
                         const double aSigma, 
                         const double aFrequency){
/////////////////////////////////////////////////////////////////////////
  inj_ra=aRa;
  inj_dec=aDec;
  inj_time=aTime;
  inj_ecc=aEccentricity;
  inj_psi=aPolarization;
  inj_amp=aAmplitude;
  inj_sigma=aSigma;
  inj_f0=aFrequency;
  InjTree->Fill();
  return (unsigned int)InjTree->GetEntries();
}

/////////////////////////////////////////////////////////////////////////
bool InjTre::Write(const string aRootFileName){
/////////////////////////////////////////////////////////////////////////
  TFile finj(aRootFileName.c_str(),"recreate");
  if(finj.IsZombie()) return false;
  finj.cd();
  InjTree->Write(InjTree->GetTitle());
  WaveTree->Fill();// single entry
  WaveTree->Write(WaveTree->GetTitle());
  finj.Close();
  return true;
}







