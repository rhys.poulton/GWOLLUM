//////////////////////////////////////////////////////////////////////////////
//  Author : florent robinet (LAL - Orsay): robinet@lal.in2p3.fr
//////////////////////////////////////////////////////////////////////////////
#ifndef __Network__
#define __Network__

#include "CUtils.h"
#include "NetConst.h"
#include "Streams.h"
#include "TMath.h"

using namespace std;

/**
 * Manage networks of streams.
 * This class was designed to combine N Streams into a network following the <a href="../../Main/convention.html#network">GWOLLUM convention</a> for networks. In particular, the N Streams can be used as N detectors of the LIGO-virgo global network. In that case many specific functions are provided to access the network properties.
 * \author    Florent Robinet
 */
class Network {
  
 public:
  
  /**
   * @name Constructors and destructors
   @{
  */
  /**
   * Constructor of the Network class.
   * A network of N Streams is instanciated.
   *
   * @param aNstreams number of streams
   * @param aVerbose verbosity level
   */
  Network(const int aNstreams, const int aVerbose=0);
  
  /**
   * Destructor of the Network class.
   */
  virtual ~Network(void);
  /**
     @}
  */

  /**
   * Returns the number of streams.
   */
  inline int GetNstreams(void) { return Nstreams; };
  
  /**
   * Returns the number of streams.
   */
  inline int GetNStreams(void) { return Nstreams; };

  /**
   * Returns the network name.
   */
  inline string GetName(void) { return NetName; };
  
  /**
   * Returns the network index.
   */
  inline int GetNetIndex(void) { return NetIndex; };

  /**
   * Returns the network type.
   */
  inline int GetType(void){ return ::GetType(NetIndex); };
  
  /**
   * Transforms the network into a LIGO-Virgo detector network.
   * A specific subnetwork is defined by the network index. See the <a href="../../Main/convention.html#network">GWOLLUM convention</a> for LIGO-Virgo networks. The network index must be compatible with the number of streams specified in the constructor. For example, Nstreams = 2 and network index = 11 is impossible.
   *
   * Please note that the convention for the network index changes with the call of this function. Before, the network index was determined by the number of streams in the network (2^N-1). Now it is given by the LV sub-network index (<2^N-1).
   * @param aNetIndex LV sub-network index to build
   */
  bool MakeLVNetwork(const int aNetIndex);

  /**
   * Tests whether a given detector index is inside the network.
   * true is returned if success.
   * @param aDetIndex detector index to test
   */
  bool IsDetIndexInNet(const int aDetIndex);

  /**
   * Returns the detector index of a given stream.
   * -1 is returned if there is no match.
   * @param aStreamIndex stream index in the network
   */
  int GetDetIndex(const int aStreamIndex);

 private:

  bool status_OK;           ///< class status
  int verbose;              ///< verbosity level

  int Nstreams;             ///< number of streams
  Streams** NetStreams;     ///< individual streams
  int NetIndex;             ///< network index

  string NetName;           // network name
  
  ClassDef(Network,0)
};

#endif


