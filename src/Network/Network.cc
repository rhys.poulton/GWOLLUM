//////////////////////////////////////////////////////////////////////////////
//  Author : florent robinet (LAL - Orsay): robinet@lal.in2p3.fr
//////////////////////////////////////////////////////////////////////////////
#include "Network.h"

ClassImp(Network)

/////////////////////////////////////////////////////////////////////////
Network::Network(const int aNstreams, const int aVerbose){ 
/////////////////////////////////////////////////////////////////////////

  // get parameters
  verbose=aVerbose;
  Nstreams=fabs(aNstreams);
  if(verbose) cout<<"Network::Network: building a "<<Nstreams<<"-streams network..."<<endl;

  // init
  status_OK=true;
  NetStreams = new Streams* [Nstreams];

  // individual streams
  for(int s=0; s<Nstreams; s++){
    gwl_ss<<"stream_"<<s; // default name
    NetStreams[s]= new Streams(gwl_ss.str(),verbose);
    gwl_ss.clear(); gwl_ss.str("");
  }
  
  // network name
  NetName="";
  for(int s=0; s<Nstreams; s++) NetName+=NetStreams[s]->GetNamePrefix();

  // network index = full network
  NetIndex=(int)pow(2.0,aNstreams)-1;

  /*
 
  // decompose the network
  int ni;
  for(int d=0; d<NET_DETMAX; d++){
    ni = (int)pow(2.0,(double)NET_INDEX[d]);
    if((NetID&ni)==ni){
      NetMask[d]=true;
      ndet++;
      NetName+=NET_PREFIX[d];
    }
    else NetMask[d]=false;
  }

  // detector response
  for(int d=0; d<NET_DETMAX; d++) ComputeDetectorResponse(d);
 
  // print info
  if(verbose){
    cout<<"Network::Network: number of detectors = "<<ndet<<endl;
    cout<<"Network::Network: selected network    = "<<NetName<<endl;
  }
  */
}
/*
/////////////////////////////////////////////////////////////////////////
Network::Network(const string aNetName, const int aVerbose){ 
/////////////////////////////////////////////////////////////////////////

  // get parameters
  status_OK=true;
  verbose=aVerbose;
  
  // init
  ndet=0;
  NetName="";
  NetID=0;

  // identify the network
  unsigned found;
  for(int d=0; d<NET_DETMAX; d++){
    found = aNetName.find(NET_PREFIX[d]);
    if (found!=(unsigned)(string::npos)){
      NetMask[d]=true;
      ndet++;
      NetName+=NET_PREFIX[d];
      NetID+=(int)pow(2.0,(double)NET_INDEX[d]);
    }
    else NetMask[d]=false;
  }

  // check network value
  if(NetID<=0||NetID>=pow(2.0,(double)NET_DETMAX)){
    cerr<<"Network::Network: the detector network "<<NetID<<" is not supported"<<endl;
    status_OK=false;
  }
 
  // detector response
  for(int d=0; d<NET_DETMAX; d++) ComputeDetectorResponse(d);
 
  // print info
  if(verbose){
    cout<<"Network::Network: number of detectors = "<<ndet<<endl;
    cout<<"Network::Network: selected network    = "<<NetName<<endl;
  }
}
*/
/////////////////////////////////////////////////////////////////////////
Network::~Network(void){
/////////////////////////////////////////////////////////////////////////
  if(verbose>2) cout<<"Network::~Network"<<endl;
  for(int s=0; s<Nstreams; s++) delete NetStreams[s];
  delete NetStreams;
}

/////////////////////////////////////////////////////////////////////////
bool Network::MakeLVNetwork(const int aNetIndex){
/////////////////////////////////////////////////////////////////////////
  if(!status_OK){
    cerr<<"Network::MakeLVNetwork: the Network object is corrupted"<<endl;
    return false;
  }
  if(Nstreams!=::GetType(aNetIndex)){
    cerr<<"Network::MakeLVNetwork: the network index does not match a "<<Nstreams<<"-network"<<endl;
    return false;
  }

  // redefine stream names (required to define a LV network)
  int s=0;
  for(int d=0; d<N_KNOWN_DET; d++){// test known detector
    if(IsStreamInNet(d,aNetIndex)){
      NetStreams[s]->SetName(DET_PREFIX[d]+":"+DET_NAME[d]);
      if(!NetStreams[s]->MakeLVDetector()) return false;
      s++;
    }
  }

  // new network name
  NetName="";
  for(int s=0; s<Nstreams; s++) NetName+=NetStreams[s]->GetNamePrefix();

  // new network index 
  // CAREFUL: the convention slightly changes
  //          the net index is now the subnet index of the LV network
  NetIndex=aNetIndex;

  if(s==Nstreams) return true;
  return false;
}

/////////////////////////////////////////////////////////////////////////
bool Network::IsDetIndexInNet(const int aDetIndex){
/////////////////////////////////////////////////////////////////////////
  for(int s=0; s<Nstreams; s++) 
    if(NetStreams[s]->GetDetIndex()==aDetIndex) return true;
  return false;
}

/////////////////////////////////////////////////////////////////////////
int Network::GetDetIndex(const int aStreamIndex){
/////////////////////////////////////////////////////////////////////////
  if(aStreamIndex<0||aStreamIndex>=Nstreams) return -1;
  return NetStreams[aStreamIndex]->GetDetIndex();
}
