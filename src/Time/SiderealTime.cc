/*
 * Copyright (C) 2007  Jolien Creighton, and Kipp Cannon
 */

#include "Date.h"

double GreenwichSiderealTime(const double aGps, const double aEquationOfEquinoxes){
  struct tm utc;
    
  /*
   * Convert GPS seconds to UTC.  This is where we pick up knowledge
   * of leap seconds which are required for the mapping of atomic
   * time scales to celestial time scales.  We deal only with integer
   * seconds.
   */
  if(!GPSToUTC(&utc, (unsigned int)aGps)) return 0.0;

  /*
   * And now to Julian day number.  Again, only accurate to integer
   * seconds.
   */
  double julian_day = JulianDay(&utc);
  
  /*
   * Convert Julian day number to the number of centuries since the
   * Julian epoch (1 century = 36525.0 days).  Here, we incorporate
   * the fractional part of the seconds.  For precision, we keep
   * track of the most significant and least significant parts of the
   * time separately.  The original code in NOVAS-C determined t_hi
   * and t_lo from Julian days, with t_hi receiving the integer part
   * and t_lo the fractional part.  Because LAL's Julian day routine
   * is accurate to the second, here the hi/lo split is most
   * naturally done at the integer seconds boundary.  Note that the
   * "hi" and "lo" components have the same units and so the split
   * can be done anywhere.
   */
  
  double t_hi = (julian_day - EPOCH_J2000_0_JD) / 36525.0;
  double t_lo = (aGps-floor(aGps))*1e9 / (1e9 * 36525.0 * 86400.0);

  /*
   * Compute sidereal time in sidereal seconds.  (magic)
   */
  double t = t_hi + t_lo;

  double sidereal_time = aEquationOfEquinoxes + (-6.2e-6 * t + 0.093104) * t * t + 67310.54841;
  sidereal_time += 8640184.812866 * t_lo;
  sidereal_time += 3155760000.0 * t_lo;
  sidereal_time += 8640184.812866 * t_hi;
  sidereal_time += 3155760000.0 * t_hi;
  
  /*
   * Return radians (2 pi radians in 1 sidereal day = 86400 sidereal
   * seconds).
   */
  
  return sidereal_time * M_PI / 43200.0;
}

double GreenwichMeanSiderealTime(const double aGps){
  return GreenwichSiderealTime(aGps, 0.0);
}
