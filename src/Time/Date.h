/**
 * @file 
 * @brief Generic timing routines.
 * @author Karl Wette, Duncan Brown, David Chin, Jolien Creighton, Kipp Cannon, Reinhard Prix, Stephen Fairhurst, Florent Robinet
 */
#ifndef _DATE_H
#define _DATE_H

#include "CUtils.h"
#include "LeapSeconds.h" /* contains the leap second table */

#define M_C  299792458.0 ///< Speed of light.
#define EPOCH_J2000_0_JD 2451545.0 ///< Julian Day of the J2000.0 epoch (2000 JAN 1 12h UTC).
#define EPOCH_UNIX_GPS 315964800
#define EPOCH_GPS_TAI_UTC 19 ///< Leap seconds (TAI-UTC) on the GPS epoch (1980 JAN 6 0h UTC) */

/**
 * @brief Returns the leap seconds TAI-UTC at a given GPS second.
 * @param[in] aGps GPS time [s].
 */
int LeapSeconds(const unsigned int aGps);

/**
 * @brief Returns the leap seconds TAI-UTC for a given UTC broken down time.
 * @param[in] aUtc Pointer to a time structure.
 */  
int LeapSecondsUTC(const struct tm *aUtc);

/**
 * @brief Returns a pointer to a tm structure representing the time
 * specified in seconds since the GPS epoch.
 * @param[out] aUtc Pointer to a time structure.
 * @param[in] aGps GPS time [s].
 */
struct tm* GPSToUTC(struct tm *aUtc, unsigned int aGps);

/**
 * @brief Returns the GPS seconds for a specified UTC time structure.
 * @param[in] aUtc Pointer to a time structure.
 */
unsigned int UTCToGPS(const struct tm *aUtc);
  
/**
 * @brief Returns the Julian Day (JD) corresponding to the date given in a broken down time structure.
 * @param[in] aUtc Pointer to a time structure.
 
 */
double JulianDay(const struct tm *aUtc);
  
/**
 * @brief Returns the time stamp from a GPS time.
 * @details Format: "%Y-%b-%d %H:%M:%S"
 * @param[in] aGps GPS time [s].
 */
string GetTimeStampFromGps(const unsigned int aGps);
  
/**
 * @brief Returns the Greenwich mean or aparent sideral time in radians.
 * @details Aparent sidereal time is computed by providing the
 * equation of equinoxes in units of seconds.  For mean sidereal time, set
 * this parameter to 0.
 *
 * This function returns the sidereal time in radians measured from the
 * Julian epoch (current J2000).  The result is NOT modulo 2 pi.
 *
 * Inspired by the function sidereal_time() in the NOVAS-C library, version
 * 2.0.1, which is dated December 10th, 1999, and carries the following
 * references:
 *
 * Aoki, et al. (1982) Astronomy and Astrophysics 105, 359-361.
 * Kaplan, G. H. "NOVAS: Naval Observatory Vector Astrometry
 *   Subroutines"; USNO internal document dated 20 Oct 1988;
 *   revised 15 Mar 1990.
 *
 * @sa http://aa.usno.navy.mil/software/novas for more information.
 * @param[in] aGps GPS time [s].
 * @param[in] aEquationOfEquinoxes Equation of equinoxes in [s]. For mean sidereal time, set this parameter to 0.
 */
double GreenwichSiderealTime(const double aGps, const double aEquationOfEquinoxes);

/**
 * @brief Returns the Greenwich Mean Sidereal Time in RADIANS for a specified GPS time.
 * @details Convenience wrapper, calling GreenwichSiderealTime() with the
 * equation of equinoxes set to 0.
 * @param[in] aGps GPS time [s].
 */
double GreenwichMeanSiderealTime(const double aGps);

#endif /* _DATE_H */
