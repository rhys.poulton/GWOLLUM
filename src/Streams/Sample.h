/**
 * @file 
 * @brief Time series transformation.
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#ifndef __Sample__
#define __Sample__

#include "CUtils.h"
#include <TMath.h>
#include <TObject.h>

#define NORDER 20
#define NORDERHP 12

using namespace std;

/**
 * @brief Produce a filter defined by a set of zeros, poles and a gain factor.
 * @details This class is designed to create and use a filter defined by zeros and poles.
 *
 * @author    Florent Robinet
 */
class ZPGFilter {

  friend class IIRFilter;

 public:

 /**
   * @name Constructors and destructors
   @{
  */

  /**
   * @brief Constructor of the ZPGFilter class.
   * @param[in] aNumZeros Number of zeros.
   * @param[in] aNumPoles Number of poles.
   */
  ZPGFilter(const unsigned int aNumZeros, const unsigned int aNumPoles);
  
  /**
   * @brief Destructor of the ZPGFilter class.
   */
  virtual ~ZPGFilter(void);
  /**
     @}
  */
  
  /**
   * @brief Move from the W- to Z-plane.
   */
  void WToZ(void);

  /**
   * @brief Set a filter pole.
   * @param[in] aPoleIndex Pole index.
   * @param[in] aPoleRe Pole real part.
   * @param[in] aPoleIm Pole imaginary part.
   */
  inline void SetPole(const unsigned int aPoleIndex,
                      const double aPoleRe, const double aPoleIm){
    poles[0][aPoleIndex] = aPoleRe;
    poles[1][aPoleIndex] = aPoleIm;
  }

   /**
   * @brief Set a filter zero.
   * @param[in] aZeroIndex Zero index.
   * @param[in] aZeroRe Zero real part.
   * @param[in] aZeroIm Zero imaginary part.
   */
  inline void SetZero(const unsigned int aZeroIndex,
                      const double aZeroRe, const double aZeroIm){
    zeros[0][aZeroIndex] = aZeroRe;
    zeros[1][aZeroIndex] = aZeroIm;
  }

   /**
   * @brief Set a filter gain.
   * @param[in] aGainRe Gain real part.
   * @param[in] aGainIm Gain imaginary part.
   */
  inline void SetGain(const double aGainRe, const double aGainIm){
    gain[0] = aGainRe;
    gain[1] = aGainIm;
  }

 
private:
  
  unsigned int nzeros; ///< Number of zeros.
  unsigned int npoles; ///< Number of poles.
  double *zeros[2];    ///< Complex zeros.
  double *poles[2];    ///< Complex poles.
  double gain[2];      ///< Complex gain.
};


/**
 * @brief Produce an infinite impulse response filter.
 * @author    Florent Robinet
 */
class IIRFilter{


public:
  
  /**
   * @name Constructors and destructors
   @{
  */
  /**
   * @brief Constructor of the IIRFilter class.
   * @param[in] aZpgFilter Pointer to a ZPG filter.
   */
  IIRFilter(ZPGFilter *aZpgFilter);

  /**
   * @brief Destructor of the IIRFilter class.
   */
  virtual ~IIRFilter(void);
  /**
     @}
  */

  /**
   * @brief Applies filter.
   * @param[in] aSize Data vector size.
   * @param[in,out] aData Pointer to data vector (first element).
   */
  void Apply(const unsigned int aSize, double *aData);


private:

  unsigned int nzeros;            ///< Number of zeros.
  unsigned int npoles;            ///< Number of poles.
  double *zeros[2];               ///< Zeros (complex).
  double *poles[2];               ///< Poles (complex).
  double gain[2];                 ///< Gain (complex).
  unsigned int numDirect;         ///< The number of direct filter coefficients.
  unsigned int numRecurs;         ///< The number of recursive filter coefficients.
  double *directCoef;             ///< The direct filter coefficients.
  double *recursCoef;             ///< The recursive filter coefficients.
  double *history;                ///< The previous values of w.
  double *history_init;           ///< The initial values of w.

};

/**
 * @brief Transform discrete time series.
 * @details A timeseries, sampled at a native frequency is transformed in the time domain. Supported transformations (in this order):
 * - Remove DC frequency
 * - High-pass filter (order=12)
 * - Window the data
 * - Resample to a working frequency
 *
 * Once the transformations are configured, call Transform() to transform your timeseries.
 * @author Florent Robinet
 */
class Sample {

 public:

  /**
   * @name Constructors and destructors
   @{
  */
  /**
   * @brief Constructor of the Sample class.
   * @param[in] aVerbosity Verbosity level.
   */
  Sample(const unsigned int aVerbosity=0);

  /**
   * Destructor of the Sample class.
   */
  virtual ~Sample(void);
  /**
     @}
  */

  /**
   * @brief Transforms data.
   * The input data vector (time-domain) is transformed in this order:
   * - DC removal: RemoveDC()
   * - Highpass: HighPass()
   * - Window the data.
   * - Resampling: Resample()
   *   
   * The input and output vectors must have an integer duration.
   * @warning The window is set with SetWindow(). The window size must match the input data size.
   *
   * @warning The memory for the output vector must be allocated with the right size before calling this function. No size check will be performed and the data size provided by the user will be trusted.
   *
   * @warning The input vector can be altered in case of presampling. Do not use after calling this function.
   * @param[in] aInSize Input vector size.
   * @param[in] aInData Input vector.
   * @param[in] aOutSize Output vector size.
   * @param[in] aOutData Output vector.
   */
  bool Transform(const unsigned int aInSize, double *aInData, const unsigned int aOutSize, double *aOutData);

  /**
   * @brief Activates/Deactivates the DC removal.
   * @details The DC component is computed and is subtracted from all data samples.
   * The final vector has a DC component equal to 0.
   * @param[in] aDCremove Activate/deactivate the DC removal.
   */
  inline void SetDCRemoval(const bool aDCremove=true){ fRemoveDC=aDCremove; };

  /**
   * @brief Defines the window.
   * @param[in] aWindowSize Window size: it must match the size of input data when calling Transform().
   * @param[in] aWindow Array of window values.
   */
  inline void SetWindow(const unsigned int aWindowSize, double *aWindow){
    fWindowSize = aWindowSize;
    fWindow = aWindow;
  };

  /**
   * @brief Returns the window size (=0 if no windowing).
   */
  inline unsigned int GetWindowSize(void){ return fWindowSize; };
  
  /**
   * @brief Defines frequencies for the transformation.
   * @details It is possible to work with a sampling frequency smaller than the native frequency, often for performance reasons.
   * It must be an integer value and a power of 2.
   * When transformed (see Transform()), the input data are first downsampled to the working frequency with an anti-aliasing filter.
   *
   * It is also possible to highpass the data before downsampling.
   * @param[in] aNativeFrequency Native sampling frequency [Hz].
   * @param[in] aWorkingFrequency Working sampling frequency [Hz].
   * @param[in] aHighPassFrequency Highpass frequency [Hz].
   * @returns true if the frequencies were successfuly changed, false otherwise.
   */
  bool SetFrequencies(const unsigned int aNativeFrequency,
                      const unsigned int aWorkingFrequency,
                      const double aHighPassFrequency=0.0);

  /**
   * @brief Sets a new native sampling frequency.
   * @param[in] aNativeFrequency New native sampling frequency [Hz].
   * @returns true if the frequency was successfuly changed, false otherwise.
  */
  inline bool SetNativeFrequency(const int aNativeFrequency){
    return SetFrequencies(aNativeFrequency,fWorkingFrequency,fHighPassFrequency);
  };

  /**
   * @brief Sets a new working sampling frequency.
   * @details It is possible to work with a sampling frequency smaller than the native frequency, often for performance reasons.
   * This function sets a new working frequency.
   * It must be an integer value and a power of 2. When transformed (see Transform()), the input data are first downsampled to the working frequency with an anti-aliasing filter.
   * @param[in] aWorkingFrequency New working sampling frequency [Hz].
   * @returns true if the frequency was successfuly changed, false otherwise.
   */
  inline bool SetWorkingFrequency(const unsigned int aWorkingFrequency){
    return SetFrequencies(fNativeFrequency,aWorkingFrequency,fHighPassFrequency);
  };

  /**
   * @brief Defines a highpass frequency.
   * @details It is possible to highpass the data.
   * This function sets a new highpass frequency.
   * When transformed (see Transform()), the input data are filtered.
   * @param[in] aHighPassFrequency Highpass frequency [Hz].
   * @returns true if the frequency was successfuly changed, false otherwise.
   */
  inline bool SetHighPassFrequency(const double aHighPassFrequency){
      return SetFrequencies(fNativeFrequency,fWorkingFrequency,aHighPassFrequency);
  };

  /**
   * @brief Returns the current native sampling frequency [Hz].
   */
  inline unsigned int GetNativeFrequency(void){ return fNativeFrequency; };

  /**
   * @brief Returns the current working sampling frequency [Hz].
   */
  inline unsigned int GetWorkingFrequency(void){ return fWorkingFrequency; };

  /**
   * @brief Returns the current highpass frequency.
   */
  inline double GetHighPassFrequency(void){ return fHighPassFrequency; };

 protected:

  bool fRemoveDC;                ///< DC removal flag.
  unsigned int fWorkingFrequency;///< Working sampling frequency.
  unsigned int fNativeFrequency; ///< Native sampling frequency.
  unsigned int fVerbosity;       ///< Verbosity level.
  double fHighPassFrequency;     ///< Cutoff frequency.
  double *fWindow;               ///< Window.
  unsigned int fWindowSize;      ///< Window size.

 private:

  bool presample;                     ///< Pre-sample flag.
  unsigned int fResampleFactor;       ///< Resampling factor (>=1).
  unsigned int fIntermediateFrequency;///< Intermediate sampling frequency.
  IIRFilter *iirFilter[NORDER];       ///< Downsampling filters.
  IIRFilter *iirFilterHP[NORDERHP];   ///< Highpass filters.

  /**
   * @brief Constructs the re-sampling filters.
   * @details Using Native frequency and Working frequency.
   */
  void MakeSamplingFilters(void);

  /**
   * @brief Constructs the highpass filters.
   * @details Using Highpass frequency and Native frequency.
   */
  void MakeHighPassFilters(void);

  /**
   * @brief Removes the DC component.
   * @param[in] aSize Data vector size.
   * @param[in,out] aData Pointer to the data vector.
   */
  void RemoveDC(const unsigned int aSize, double *aData);

  /**
   * @brief Highpasses the data.
   * @details The data vector, sampled at the native frequency, is highpassed.
   * @param[in] aSize Data vector size.
   * @param[in,out] aData Pointer to the data vector.
   */
  void HighPass(const unsigned int aSize, double *aData);

  /**
   * @brief Pre-samples the data.
   * @details The input data vector is resampled to the power-of-two just below.
   * @note The output vector size does not change. However, the returned size gets smaller!
   * @param[in,out] aSize Data vector size.
   * @param[in,out] aData Pointer to the data vector.
   */
  void PreSample(unsigned int &aSize, double *aData);

  /**
   * @brief Re-samples the data.
   * @details The input data vector, sampled at the native frequency,
   * is resampled to the working frequency.
   * @warning The output vector must be the right size (fWorkingFrequency)!
   * @param[in] aSize Input data vector size.
   * @param[in] aInData Pointer to the input data vector.
   * @param[out] aOutData Pointer to the output data vector.
   */
  void Resample(unsigned int aSize, double *aInData, double *aOutData);
  
  ClassDef(Sample,0)
};

#endif


