/**
 * @file 
 * @brief Frame file access.
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#ifndef __ffl__
#define __ffl__

#include "GwollumPlot.h"
#include "Segments.h"
#include <FrameL.h>
#include <FrVect.h>

using namespace std;

/**
 * @brief Load frame files and access data in frames.
 * @details This class is designed to load frame files and provide methods to access the data.
 * The native format for file registration is the so-called FFL format.
 * A FFL file is a text file where each line identifies a frame file.
 * Each line must contained exactly 5 columns:
 * - path to a frame file
 * - starting GPS time of the frame file
 * - duration of the frame file [s]
 * - unused column [0]
 * - unused column [0]
 *
 * @note The FFL format is the native format for this class. However the lalcache format (LCF) is also supported. In that case the LCF file is first converted to a FFL file before processing.
 *
 * This class is implemented using the <a href="http://lappweb.in2p3.fr/virgo/FrameL/">Frame Library</a> functions.
 *
 * @author Florent Robinet
 */
class ffl {

 public:

  /**
   * @name Constructors and destructors
   @{
  */
  /**
   * @brief Constructor of the ffl class.
   * @details The frame file list is provided.
   * Two format are supported: the native Frame File List (FFL) and the lalcache format (LCF). If a LCF file is provided, this file is first converted to a FFL in the current directory (LoadFrameFile()).
   * @param aFrameFileList path to frame file list
   * @param aPlotStyle plotting style
   * @param aVerbosity verbosity level
   */
  ffl(const string aFrameFileList,
      const string aPlotStyle="GWOLLUM",
      const int aVerbosity=0);

  /**
   * @brief Destructor of the ffl class.
   */
  virtual ~ffl(void);
  /**
     @}
  */

  /**
   * @brief Resets ffl object.
   */
  void Reset(void);

  /**
   * @brief Sets a new name for the plotting structure.
   * @param[in] aName Plot name.
   */
  void SetPlotName(const string aName);

  /**
   * @brief Defines a temporary working directory.
   * @details When working with another file format than the native FFL, a conversion to FFL is required.
   * The converted file is saved in a working directory defined with this function.
   * The directory must exist before calling this function, or else the current directory is used.
   * @param[in] aTmpDirPath Path to the working directory.
   */
  void DefineTmpDir(const string aTmpDirPath=".");
  
  /**
   * @brief Returns the path to the input frame file list.
   */
  inline string GetInputFfl(void){ return fInFrameFile; };

  /**
   * @brief Sets the number of trials to load the frame file.
   * @details When calling the LoadFrameFile() function, several attemps are made to access the frame file list.
   * This is useful for online applications when the FFL file is updated.
   * @param[in] aNTrials Number of trials.
   * @param[in] aNSleepSeconds Number of seconds between trials.
   */
  inline void SetTrials(const unsigned int aNTrials, const unsigned int aNSleepSeconds){
    ntrials = aNTrials; nsleepsec = aNSleepSeconds;
  };

  /**
   * @brief Converts the frame file list to a FFL format.
   * @details If the format is different from the native FFL format, the frame file list is converted into a working FFL file in the tmp directory (set with DefineTmpDir()).
   *
   * @returns The name of the converted file is returned. "none" is returned if this function fails.
   */
  string Convert2Ffl(void);

  /**
   * @brief Extracts channels at a given GPS time.
   * @details Only ADC, PROC, SIM and SER channels are extracted.
   * Only channels with a sampling rate above or equal to 1Hz are extracted.
   *
   * @note The data is retrieved several times, as configured with SetTrials().
   * @note LoadFrameFile() must be called at least one time before calling this function.
   * @param[in] aGps GPS time where to scan the channel list.
   * @returns true if the channel list was correctly loaded, false otherwise.
   */
  bool ExtractChannels(const unsigned int aGps);

/**
   * @brief Loads (or re-loads) the frame file list.
   * @details If the format is different from the native FFL format, the frame file list is first converted into a working FFL file in the tmp directory defined with DefineTmpDir().
   * This function loads the working ffl file. It is possible to configure several trials to load the FFL file (see SetTrials()).
   * The channel information is extracted at a given GPS time using ExtractChannels().
   *
   * This function can also be used if the frame file list needs to be reloaded (for example if it has changed on disk).
   *
   * @param[in] aGps GPS time where to scan the channel list. Use 0 to scan the first frame file in the list.
   * @returns true if the frame file list was correctly loaded, false otherwise.
   */
  bool LoadFrameFile(const unsigned int aGps=0);

  /**
   * @brief Returns a pointer to the segments covered by the ffl.
   * @warning DO NOT DELETE OR MODIFY! This object is internally used by the class.
   */
  inline Segments* GetSegments(void){ return seg; }

  /**
   * @brief Returns the sampling rate of a given channel [Hz].
   * @returns 0 if the channel is not found.
   * @note To target a given GPS time, use ExtractChannels() first.
   * @param[in] aChannelName Channel name.
   * @param[out] aChannelIndex Channel index.
   */
  unsigned int GetChannelSampling(const string aChannelName, unsigned int &aChannelIndex);

  /**
   * @brief Returns the sampling rate of a given channel [Hz].
   * @returns 0 if the channel is not found.
   * @note To target a given GPS time, use ExtractChannels() first.
   * @param[in] aChannelName Channel name.
   */
  inline unsigned int GetChannelSampling(const string aChannelName){
    unsigned int dummy;
    return GetChannelSampling(aChannelName, dummy);
  };

  /**
   * @brief Prints the list of channels at a given GPS time.
   * @details Two columns are printed: the channel name and the sampling frequency [Hz].
   * @param[in] aGps GPS time.  
   */
  inline bool PrintChannels(const unsigned int aGps=0){
    bool res=LoadFrameFile(aGps);
    for(unsigned int l=0; l<channels.size(); l++)
      cout<<channels[l]<<" "<<sampling[l]<<endl;
    return res;
  };

  /**
   * @brief Returns the current channel list.
   * @details It is possible to return a subset of channels using a filter.
   * This filter is a string containing wildcards, e.g. "V1:DQ_*_FLAG_* V1:ENV_*".
   * @param[in] aFilter Channel name filter. Use "" for no filter.
   */  
  vector <string> GetChannelList(const string aFilter="");

  /**
   * @brief Tests whether a channel exists in the dataset pointed by the ffl.
   * @returns true if the channel exists, false otherwise.
   * @warning Make sure that the channel list was extracted at the correct GPS time. See ExtractChannels() or LoadFrameFile().
   * @param[in] aChannelName Channel name to test.
   */
  bool IsChannel(const string aChannelName);

 /**
   * @brief Returns a data vector for a given channel and a GPS range.
   * @details A pointer to the data vector is returned.
   * The size of the vector is returned.
   * If the data cannot be read, a pointer to NULL is returned and the data size is set to 0.
   * @note The data access is tried several times, as configured with SetTrials().
   *
   * If there are missing data in the requested time stretch, no data vector is returned by default (`aMissing=999.0`). For any other value for `aMissing`, the data vector is returned and the missing sample are given the value `aMissing`.
   *
   * @note The user is in charge of deleting the returned vector.
   * @param[out] aSize Number of samples in the returned vector.
   * @param[in] aChannelName Channel name.
   * @param[in] aGpsStart Starting GPS time.
   * @param[in] aGpsEnd Ending GPS time.
   * @param[in] aMissing Special value to flag missing samples. If =999.0, no flagging, the function returns NULL.
   */
  double* GetData(unsigned int &aSize, const string aChannelName,
                  const double aGpsStart, const double aGpsEnd,
                  const double aMissing=999.0);

  /**
   * @brief Prints a data vector in a text file.
   * @details This function prints the time series of a given channel between two GPS times in an text file. 
   * @param[in] aChannelName Channel name.
   * @param[in] aGpsStart Starting GPS time.
   * @param[in] aGpsEnd Ending GPS time.
   * @param aFileName Output text file name.
   */
  bool PrintData(const string aChannelName, const double aGpsStart, const double aGpsEnd, string aFileName);

  /**
   * @brief Plots a data vector time series.
   * @details This function, when run interactively, plots the time series of a given channel between two GPS times.
   * @returns A pointer to the resulting graph is returned. Do not modify or delete this object as it is internally used by the class. NULL is returned if this function fails.
   * @param[in] aChannelName Channel name.
   * @param[in] aGpsStart Starting GPS time.
   * @param[in] aGpsEnd Ending GPS time.
   */
  TGraph* PlotData(const string aChannelName, const double aGpsStart, const double aGpsEnd);
  
 private:

  bool status_OK;          ///< Class status.
  string fInFrameFile;     ///< Path to input frame file list.
  string fWoFrameFile;     ///< Path to working frame file (always FFL).
  string fFormat;          ///< Format frame file.
  unsigned int fVerbosity; ///< Verbosity level.
  string srandint;         ///< Random integer string.
  unsigned int ntrials;    ///< Number of trials when accessing the data.
  unsigned int nsleepsec;  ///< Number of seconds between trials.

  vector<string> ffile;    ///< List of files.
  Segments *seg;           ///< Segments covered by the ffl file.
  FrFile *frfile;          ///< Fr file.
  vector <string> channels;///< List of channels.
  vector <unsigned int> sampling;///< List of sampling frequencies.

  // PLOTS
  GwollumPlot *GP;         ///< Plotting structure.
  TGraph *G;               ///< Graph.

  ClassDef(ffl,0)  

};

#endif


