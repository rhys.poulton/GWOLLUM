/**
 * @file 
 * @brief Program to print the power/amplitude spectral density of a time series.
 * @snippet this gwl-print-psd-usage 
 *
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "ffl.h"
#include "Spectrum.h"

using namespace std;

/**
 * @brief Main program.
 */
int main(int argc, char* argv[]){

  if(argc>1&&!((string)argv[1]).compare("version")){
    return GwlPrintVersion();
  }

  // check the argument
  if(argc<5){
     //! [gwl-print-psd-usage]
    cerr<<"This program computes and plots the power/amplitude spectral density of a given channel."<<endl;
    cerr<<"A FFL (or LCF) file must be provided, as well as a GPS range."<<endl;
    cerr<<"The PSD (and ASD) of a given channel is computed and saved as a TGraph in a ROOT file."<<endl;
    cerr<<"Optionally the frequency resolution can be specified (=1 Hz by default)"<<endl;
    cerr<<""<<endl;
    cerr<<"usage:"<<endl;
    cerr<<argv[0]<<" [ffl file] [channel name] [gps start] [gps end] ([PSD frequency resolution])"<<endl; 
    cerr<<""<<endl;
    cerr<<"[ffl file]                 Path to FFL (or LCF) file."<<endl;
    cerr<<"[channel name]             Channel name."<<endl;
    cerr<<"[gps start]                GPS start."<<endl;
    cerr<<"[gps end]                  GPS end."<<endl;
    cerr<<"[PSD frequency resolution] Spectrum frequency resolution in [Hz]."<<endl;
    cerr<<""<<endl;
    //! [gwl-print-psd-usage]
    return 1;
  }

  // channel name
  string channelname = (string)argv[2];

  // time range
  unsigned int gps_start = atoi(argv[3]);
  unsigned int gps_end = atoi(argv[4]);
  unsigned int timerange=gps_end-gps_start;

  // load ffl file
  ffl *FFL = new ffl((string)argv[1], "STANDARD", 0);
  if(!FFL->LoadFrameFile(gps_start)) return 2;

  // channel sampling frequency
  unsigned int channelsampling = FFL->GetChannelSampling(channelname);
  if(!channelsampling) return 3;

  // PSD resolution
  double df = 1.0; // Hz
  if(argc>5) df=atof(argv[5]);
  unsigned int N = NextPowerOfTwo((double)channelsampling/2.0/df);

  // Spectrum
  Spectrum *S = new Spectrum(N, timerange, channelsampling, 2);

  // get data
  unsigned int dsize = 0;
  double *data=FFL->GetData(dsize, channelname, gps_start, gps_end);
  if(dsize!=timerange*channelsampling) return 4;

  // compute PSD
  S->AddData(dsize, data);
  delete data;
    
  TGraph *P = S->GetPSD();
  TGraph *A = S->GetASD();

  gwl_ss<<"mypsd_"<<ReplaceAll(channelname,":","-")<<"-"<<gps_start<<"-"<<gps_end-gps_start<<".root";
  TFile *fout = new TFile(gwl_ss.str().c_str(),"recreate");
  gwl_ss.clear(); gwl_ss.str("");
  fout->cd();
  P->Write();
  A->Write();
  fout->Close();

  delete P;
  delete A;
  delete S;
  delete FFL;
  
  return 0;
}

