/**
 * @file 
 * @brief Program to print the segments of trigger files.
 * @snippet this gwl-print-livetime-usage 
 *
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "ReadTriggerMetaData.h"

using namespace std;

/**
 * @brief Main program.
 */
int main (int argc, char* argv[]){

  if(argc>1&&!((string)argv[1]).compare("version")){
    return GwlPrintVersion();
  }

  // check the argument
  if(argc<2){
    //! [gwl-print-livetime-usage]
    cerr<<"This program prints the segments of trigger files."<<endl;
    cerr<<endl;
    cerr<<"usage:"<<endl;
    cerr<<"gwl-print-livetime [ROOT trigger files] ([GPS start] [GPS end])"<<endl; 
    cerr<<endl;
    cerr<<"  [ROOT trigger files] : list of ROOT trigger files (pattern)"<<endl; 
    cerr<<"  [GPS start]: GPS start time (optional)"<<endl; 
    cerr<<"  [GPS end]: GPS end time (optional)"<<endl; 
    cerr<<""<<endl;
    //! [gwl-print-livetime-usage]
    return 1;
  }

  // arguments
  string infiles = (string)argv[1];
  Segments *seg = NULL;
  if(argc>=4)
    seg = new Segments(atof(argv[2]), atof(argv[3]));

  // input triggers
  ReadTriggerMetaData *intriggers = new ReadTriggerMetaData(infiles,"",0);

  // w/ input segments
  if((seg!=NULL) && seg->GetStatus() && seg->GetN()){
    seg->Intersect(intriggers);
    seg->Segments::Dump();
    delete intriggers;
    delete seg;
    return 0;
  }

  // w/o input segments
  intriggers->Segments::Dump();

  // cleaning
  delete intriggers;

  return 0;
}

