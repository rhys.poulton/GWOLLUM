/**
 * @file 
 * @brief Write triggers to disk.
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">fl
orent.robinet@ijclab.in2p3.fr</a>
 */
#ifndef __MakeTriggers__
#define __MakeTriggers__

#include <H5Cpp.h>
#include "Streams.h"
#include "Triggers.h"

using namespace std;

/**
 * @brief Write triggers to disk.
 * @details This class is designed to write triggers in a ROOT tree (other supported formats: hdf5, xml, txt).
 * When triggers are produced by some external algorithms, the MakeTriggers class can be used to manage output triggers and to save them to disk.
 * 
 * The user is responsible to fill both the Segments and the Triggers structure from which this class inherits.
 * @author Florent Robinet
 */
class MakeTriggers: public Triggers, public Segments , public Streams {

 public:

  /**
   * @name Constructors and destructors
   @{
  */
  /**
   * @brief Constructor of the MakeTriggers class.
   * @details The different structures are initialized:
   * - Streamsobject
   * - Triggers object
   * - Segments object
   *
   * @param[in] aStreamName Stream name.
   * @param[in] aVerbose Verbosity level.
   */
  MakeTriggers(const string aStreamName, const unsigned int aVerbose=0);

  /**
   * @brief Destructor of the MakeTriggers class.
   */
  virtual ~MakeTriggers(void);
  /**
     @}
  */
  
  /**
   * @brief Resets all.
   * @details The memory is cleaned up:
   * - triggers/clusters (Triggers::Reset())
   * - segments
   * - metadata
   */
  void Reset(void);
   
  /**
   * @brief Writes triggers to disk.
   * @details This function writes all the triggers added until now in an output file as well as the segments and metadata.
   * The MakeTriggers object is then completely reset (see Triggers()).
   *
   * @returns The output file path is returned. An empty string is returned if this function fails in some way.
   *
   * Several output formats are supported:
   * - 'root': standard GWOLLUM root trigger file
   * - 'hdf5': hdf5 format
   * - 'xml': LIGO xml sngl_burst table
   * - 'txt': text file with columns
   *
   * Multiple output formats can be requested at once. Use a single string to list the requested formats.
   * @note For the hdf5, xml and txt formats, if triggers were clustered, only clusters are saved. For the root format, only triggers are saved.
   *
   * The optional argument 'aFileName' is used to bypass the file naming convention of GWOLLUM. With this option, the trigger file will be given a new name: [aFileName].[format]
   *
   * For ROOT files, it is possible to write the trees in a ROOT sub-directory. Use the optional argument 'aSubDir'. Only in this case, the trigger file can be updated when adding several directories. To do this, use aWriteMode="UPDATE".
   * @param[in] aOutDir Output directory (must exist).
   * @param[in] aFileFormat List of file formats.
   * @param[in] aFileName New name for ouput file. Use "" for GWOLLUM convention.
   * @param[in] aSubDir ROOT sub-directory name to write the triggers (only for ROOT format).
   * @param[in] aWriteMode Write mode: "RECREATE" or "UPDATE" (only with sub-directories).
   * @param[in] aWriteMeta Flag to write (or not) the metadata tree.
   */
  string Write(const string aOutDir=".",
	       const string aFileFormat="",
	       const string aFileName="",
	       const string aSubDir="",
	       const string aWriteMode="RECREATE",
	       const bool aWriteMeta=true);

  /**
   * @brief Set the process name.
   * @param[in] aProcessName Process name.
   */
  inline void SetProcessName(const string aProcessName){
    Mprocessname=aProcessName;
  };

  /**
   * @brief Sets the version for this process.
   * @param[in] aProcessVersion Process version.
   */
  inline void SetProcessVersion(const string aProcessVersion){
    Mprocessversion=aProcessVersion;
  };

  /**
   * @brief Initializes the user-defined metadata tree.
   * @param[in] aMetaName Vector of metadata names.
   * @param[in] aMetaType Vector of metadata types: "u" (unsigned), "i" (signed), "d" (double), or "s" (string).
   */
  void InitMetaData(vector<string> aMetaName, vector<string> aMetaType);

  /**
   * @brief Sets a user-defined metadata value (double).
   * @param[in] aMetaName Metadata name.
   * @param[in] aMetaValue Metadata value.
   */
  void SetMetaData(const string aMetaName, const double aMetaValue);

  /**
   * @brief Sets a user-defined metadata value (int).
   * @param[in] aMetaName Metadata name.
   * @param[in] aMetaValue Metadata value.
   */
  void SetMetaData(const string aMetaName, const int aMetaValue);

  /**
   * @brief Sets a user-defined metadata value (unsigned int).
   * @param[in] aMetaName Metadata name.
   * @param[in] aMetaValue Metadata value.
   */
  void SetMetaData(const string aMetaName, const unsigned int aMetaValue);

  /**
   * @brief Sets a user-defined metadata value (string).
   * @param[in] aMetaName Metadata name.
   * @param[in] aMetaValue Metadata value.
   */
  void SetMetaData(const string aMetaName, const string aMetaValue);


 protected:

  // METADATA
  TTree *Mtree;               ///< Metadata tree.
  string Mprocessname;        ///< Meta process name.
  string Mprocessversion;     ///< Meta process version.
  string Mprocessuser;        ///< Meta user name.
  double Mstart;              ///< Meta start time.
  double Mend;                ///< Meta end time.
  double Mfmin;               ///< Meta minimum fstart.
  double Mfmax;               ///< Meta maximum fend.
  double Mqmin;               ///< Meta minimum Q.
  double Mqmax;               ///< Meta maximum Q.
  double Msnrmin;             ///< Meta minimum SNR.
  double Msnrmax;             ///< Meta maximum SNR.
  vector <string> metaname;   ///< Metadata name.
  vector <string> metatype;   ///< Metadata type.
  double *Mdvar;              ///< User metatree var - double.
  unsigned int *Muvar;        ///< User metatree var - usigned int.
  int    *Mivar;              ///< User metatree var - int.
  string *Msvar;              ///< User metatree var - string.
 
 private:
  
  /**
   * @brief Trigger structure for hdf5.
   */
  typedef struct h5_triggers{
    double h5_time;
    double h5_freq;
    double h5_snr;
    double h5_q;
    double h5_amp;
    double h5_ph;
    double h5_tstart;
    double h5_tend;
    double h5_fstart;
    double h5_fend;
  } h5_triggers; ///< trigger structure for hdf5

  /**
   * @brief Segment structure for hdf5.
   */
  typedef struct h5_segments{
    double h5_start;
    double h5_end;
  } h5_segments;

  /**
   * @brief Datatype for hdf5 (triggers).
   */
  H5::CompType *h5_type;

  /**
   * @brief Datatype for hdf5 (segments).
   */
  H5::CompType *h5_type_s;

  /**
   * @brief Write triggers in a HDF5 file.
   * @param[in] aFileName File name.
   */
  void WriteHDF5(const string aFileName);

  /**
   * @brief Write triggers in a text file.
   * @param[in] aFileName File name.
   */
  void WriteTXT(const string aFileName);

  /**
   * @brief Write triggers in a XML file.
   * @param[in] aFileName File name.
   */
  void WriteXML(const string aFileName);

  ClassDef(MakeTriggers,0)  
};

#endif


