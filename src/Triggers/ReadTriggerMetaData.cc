/**
 * @file 
 * @brief See ReadTriggerMetaData.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "ReadTriggerMetaData.h"

ClassImp(ReadTriggerMetaData)

////////////////////////////////////////////////////////////////////////////////////
ReadTriggerMetaData::ReadTriggerMetaData(const string aPattern, const string aDirectory,
                                         const unsigned int aVerbose):
Segments(aPattern, aDirectory, "segments"), Streams("00:none"),
  Verbose(aVerbose){
////////////////////////////////////////////////////////////////////////////////////
  gErrorIgnoreLevel = 3000;

  pattern = aPattern;
  subdir = aDirectory;
  
  // chain metadata trees...
  string treename = "metadata";
  if(aDirectory.compare(""))  treename = aDirectory+"/"+treename;
  TChain *Mtree = new TChain(treename.c_str());

  // get list of patterns and load it
  vector <string> patlist = SplitString(aPattern,' ');
  for(unsigned int p=0; p<patlist.size(); p++) Mtree->Add(patlist[p].c_str());
  patlist.clear();

  // load first tree if any
  if(Mtree->GetNtrees()>0) Mtree->LoadTree(0);

  // mandatory metadata: start / end
  double st, en, fmin, fmax, qmin, qmax, snrmin, snrmax;
  string *pn=0, *sn=0, *vn=0, *un=0;
  if(Mtree->SetBranchAddress("start", &st) != 0) st=0.0;
  if(Mtree->SetBranchAddress("end", &en) != 0) en=0.0;
  if(Mtree->SetBranchAddress("Mfmin", &fmin) != 0) fmin=0.0;
  if(Mtree->SetBranchAddress("Mfmax", &fmax) != 0) fmax=0.0;
  if(Mtree->SetBranchAddress("Mqmin", &qmin) != 0) qmin=0.0;
  if(Mtree->SetBranchAddress("Mqmax", &qmax) != 0) qmax=0.0;
  if(Mtree->SetBranchAddress("Msnrmin", &snrmin) != 0) snrmin=0.0;
  if(Mtree->SetBranchAddress("Msnrmax", &snrmax) != 0) snrmax=0.0;
  if(Mtree->SetBranchAddress("Mprocessname", &pn) != 0) pn = new string("PROCESS");
  if(Mtree->SetBranchAddress("Mstreamname", &sn) != 0) sn = new string("00:none");
  if(Mtree->SetBranchAddress("Mprocessversion", &vn) != 0) vn = new string("0");
  if(Mtree->SetBranchAddress("Mprocessuser", &un) != 0) un = new string("unknown");

  // unknown strings
  char *login = getlogin();
  if((!(*un).compare("unknown")) && (login!=NULL)) *un=(string)login;

  // allocate memory
  unsigned int Mn = (unsigned int)Mtree->GetEntries();
  Mstart  = new double[Mn];
  Mend    = new double[Mn];
  Mfmin   = new double[Mn];
  Mfmax   = new double[Mn];
  Mqmin   = new double[Mn];
  Mqmax   = new double[Mn];
  Msnrmin = new double[Mn];
  Msnrmax = new double[Mn];

  // abs values
  Mfmin_stat   = 1e20;
  Mfmax_stat   = 0.0;
  Mqmin_stat   = 1e20;
  Mqmax_stat   = 0.0;
  Msnrmin_stat = 1e20;
  Msnrmax_stat = 0.0;
  Mprocessname = *pn;
  Streams::SetName(*sn);
  
  // read metadata
  for(unsigned int m=0; m<Mn; m++){
    Mtree->GetEntry(m);

    // consider only first process name
    if(!m) Mprocessname=*pn;
    if((*pn).compare(Mprocessname)) continue;

    // consider only first stream name
    if(!m) Streams::SetName(*sn);
    if((*sn).compare(Streams::GetName())) continue;

    Mstart[m] = st;
    Mend[m] = en;
    Mfmin[m] = fmin;
    Mfmax[m] = fmax;
    Mqmin[m] = qmin;
    Mqmax[m] = qmax;
    Msnrmin[m] = snrmin;
    Msnrmax[m] = snrmax;

    // absolute min/max
    // no triggers (-1 are used by MakeTriggers)
    if(fmin>=0){
      if(Mfmin[m]<Mfmin_stat) Mfmin_stat=Mfmin[m];
      if(Mfmax[m]>Mfmax_stat) Mfmax_stat=Mfmax[m];
      if(Mqmin[m]<Mqmin_stat) Mqmin_stat=Mqmin[m];
      if(Mqmax[m]>Mqmax_stat) Mqmax_stat=Mqmax[m];
      if(Msnrmin[m]<Msnrmin_stat) Msnrmin_stat=Msnrmin[m];
      if(Msnrmax[m]>Msnrmax_stat) Msnrmax_stat=Msnrmax[m];
    }
  }

  // default values
  if(Mfmin_stat==1e20) Mfmin_stat=0.0;
  if(Mqmin_stat==1e20) Mqmin_stat=0.0;
  if(Msnrmin_stat==1e20) Msnrmin_stat=0.0;
  if(Mfmax_stat==0.0) Mfmax_stat=1.0;
  if(Mqmax_stat==0.0) Mqmax_stat=1.0;
  if(Msnrmax_stat==0.0) Msnrmax_stat=1.0;

  // only save last user name and process version!
  Mprocessversion=(*vn);
  Mprocessuser=(*un);

  delete pn;
  delete sn;
  delete vn;
  delete un;
  delete Mtree;
}

////////////////////////////////////////////////////////////////////////////////////
ReadTriggerMetaData::~ReadTriggerMetaData(void){
////////////////////////////////////////////////////////////////////////////////////
  if(Verbose>1) cout<<"ReadTriggerMetaData::~ReadTriggerMetaData"<<endl;
  delete [] Mstart;
  delete [] Mend;
  delete [] Mfmin;
  delete [] Mfmax;
  delete [] Mqmin;
  delete [] Mqmax;
  delete [] Msnrmin;
  delete [] Msnrmax;
}

////////////////////////////////////////////////////////////////////////////////////
string ReadTriggerMetaData::Merge(const string aOutputDirectory){
////////////////////////////////////////////////////////////////////////////////////

  // missing output directory
  if(!filesystem::is_directory(aOutputDirectory)) return "";

  // no segments --> do nothing
  if(Segments::GetLiveTime()<=0) return "";

  if(Verbose>0)
    cout<<"ReadTriggerMetaData::Merge: merging triggers between "<<Segments::GetFirst()<<" "<<Segments::GetLast()<<endl;
  
  // output file
  string outfile = Streams::GetTriggerFileName((unsigned int)Segments::GetFirst(),
                                               (unsigned int)ceil(Segments::GetLast())-(unsigned int)floor(Segments::GetFirst()),
                                               "root",
                                               ReadTriggerMetaData::GetProcessName(),
                                               aOutputDirectory);

  // get trees
  TTree *segtree = Segments::GetTree();
  string treename = "metadata";
  if(subdir.compare(""))  treename = subdir+"/"+treename;
  TChain *metatree = new TChain(treename.c_str());
  treename = "triggers";
  if(subdir.compare(""))  treename = subdir+"/"+treename;
  TChain *triggertree = new TChain(treename.c_str());

  // get list of patterns and load it
  vector <string> patlist = SplitString(pattern,' ');
  for(unsigned int p=0; p<patlist.size(); p++){
    metatree->Add(patlist[p].c_str());
    triggertree->Add(patlist[p].c_str());
  }
  patlist.clear();

  TFile *fout;

  // there is some triggers
  if(triggertree->GetEntries()){

    // merge trigger tree
    if(triggertree->Merge(outfile.c_str())!=1){
      cerr<<"ReadTriggerMetaData::Merge: merge failed (triggers)"<<endl;
      return "";
    }
    
    // add metadata tree
    fout = TFile::Open(outfile.c_str(), "UPDATE");
    if(fout->IsZombie()){
      cerr<<"ReadTriggerMetaData::Merge: cannot open "<<outfile<<endl;
      return "";
    }
    fout->cd();
    if(metatree->Merge(fout,0)!=1){
      cerr<<"ReadTriggerMetaData::Merge: merge failed (metadata)"<<endl;
      return "";
    }


    // add segment tee
    fout = TFile::Open(outfile.c_str(), "UPDATE");
    if(fout->IsZombie()){
      cerr<<"ReadTriggerMetaData::Merge: cannot open "<<outfile<<endl;
      return "";
    }
    fout->cd();
    segtree->Write();
    fout->Close();
  }

  // there is no triggers
  else{
    TTree *empty = triggertree->GetTree();
    empty->SetName("triggers");
    fout = TFile::Open(outfile.c_str(), "UPDATE");
    if(fout->IsZombie()){
      cerr<<"ReadTriggerMetaData::Merge: cannot open "<<outfile<<endl;
      return "";
    }
    fout->cd();
    empty->Write();
    segtree->Write();
    if(metatree->Merge(fout,0)!=1){
      cerr<<"ReadTriggerMetaData::Merge: merge failed (metadata)"<<endl;
      return "";
    }
    delete empty;
  }

  delete triggertree;
  delete metatree;
  delete segtree;

  return outfile;
}
