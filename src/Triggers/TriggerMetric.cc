//////////////////////////////////////////////////////////////////////////////
//  Author : florent robinet (LAL - Orsay): robinet@lal.in2p3.fr
//////////////////////////////////////////////////////////////////////////////
#include "TriggerMetric.h"

ClassImp(TriggerMetric)

////////////////////////////////////////////////////////////////////////////////////
TriggerMetric::TriggerMetric(const string aPattern, const string aDirectory, const int aVerbose,
			     const double aDt, const double aSNRthreshold):
EventMap (1, aPattern, aDirectory, aVerbose){ 
////////////////////////////////////////////////////////////////////////////////////

  // clusterize
  dt=aDt;
  SetClusterizeDt(dt);
  SetClusterizeSNRthr(aSNRthreshold);
  Clusterize(-1);

  cl_list.clear();
  distance_n=0;
  distance_mean=0.0;
  distance_var=0.0;
}

////////////////////////////////////////////////////////////////////////////////////
TriggerMetric::~TriggerMetric(void){
////////////////////////////////////////////////////////////////////////////////////
  cl_list.clear();
}


////////////////////////////////////////////////////////////////////////////////////
void TriggerMetric::ComputeMetric(TObject *aFunc, const double aTimeStart, const double aTimeEnd, TH1D* aMetric){
////////////////////////////////////////////////////////////////////////////////////

  distance_mean=0.0;
  distance_var=0.0;
  TGraph *Gtmp=NULL;
  TF1 *Ftmp=NULL;

  if(!strcmp(aFunc->ClassName(),"TF1"))
    Ftmp=(TF1*)aFunc;
  else if(!strcmp(aFunc->ClassName(),"TGraph")){
    Gtmp=(TGraph*)aFunc;
    Gtmp->SetBit(TGraph::kIsSortedX);
  }
  else{
    cerr<<"TriggerMetric::ComputeMetric: only TGraph or TF1 objects are supported"<<endl;
    return;
  }
  
  // get list of overlapping clusters
  GetOverlappingClusters(aFunc, aTimeStart, aTimeEnd);

  double sum=0, sum2=0, sumw=0, weight;
  double func_time;
  int firsttrigger;
  distance_n=0; 

  // loop over overlapping clusters
  for(int c=0; c<(int)cl_list.size(); c++){
    firsttrigger=GetClusterFirstTrigger(cl_list[c]);
    for(int t=0; t<GetClusterSize(cl_list[c]); t++){
      if(Ftmp!=NULL) func_time=Ftmp->Eval(GetTriggerFrequency(firsttrigger+t));
      else func_time=Gtmp->Eval(GetTriggerFrequency(firsttrigger+t));
      if(func_time<aTimeStart) continue;
      if(func_time>aTimeEnd) continue;
      weight=GetTriggerSNR(firsttrigger+t)*GetTriggerSNR(firsttrigger+t);
      sum+=((GetTriggerTime(firsttrigger+t)-func_time)*weight);
      sum2+=((GetTriggerTime(firsttrigger+t)-func_time)*(GetTriggerTime(firsttrigger+t)-func_time)*weight);
      sumw+=weight;
      distance_n++;

      if(aMetric!=NULL) aMetric->Fill(GetTriggerTime(firsttrigger+t)-func_time,weight);
    }
  }

  // compute metric
  if(sumw){
    distance_mean=sum/sumw;
    distance_var=sum2/sumw-distance_mean*distance_mean;
  }

  return;
}

////////////////////////////////////////////////////////////////////////////////////
void TriggerMetric::GetOverlappingClusters(TObject *aFunc, const double aTimeStart, const double aTimeEnd){
////////////////////////////////////////////////////////////////////////////////////

  TGraph *Gtmp=NULL;
  TF1 *Ftmp=NULL;

  if(!strcmp(aFunc->ClassName(),"TF1"))
    Ftmp=(TF1*)aFunc;
  else if(!strcmp(aFunc->ClassName(),"TGraph")){
    Gtmp=(TGraph*)aFunc;
    Gtmp->SetBit(TGraph::kIsSortedX);
  }
  else{
    cerr<<"TriggerMetric::GetOverlappingClusters: only TGraph or TF1 objects are supported"<<endl;
    return;
  }

  cl_list.clear();

  int firsttrigger;
  bool toadd;
  double func_time;
  
  // loop over clusters
  for(int c=0; c<GetNClusters(); c++){

    // before --> skip
    if(GetClusterTimeEnd(c)+dt<aTimeStart) continue;
    
    // after --> stop
    if(GetClusterTimeStart(c)-dt>aTimeEnd) break;

    // first trigger in the cluster
    firsttrigger=GetClusterFirstTrigger(c);
    toadd=false;
    
    // loop over triggers
    for(int t=0; t<GetClusterSize(c); t++){
      if(Ftmp!=NULL) func_time=Ftmp->Eval(GetTriggerFrequency(firsttrigger+t));
      else func_time=Gtmp->Eval(GetTriggerFrequency(firsttrigger+t));
      if(func_time>GetTriggerTimeEnd(firsttrigger+t)+dt) continue;// trigger is before
      if(func_time<GetTriggerTimeStart(firsttrigger+t)-dt) continue;// trigger is after
      toadd=true;// overlapping
      break;
    }

    // add cluster to the list
    if(toadd){
      cl_list.push_back(c);
    }
    
  }

  return;
}

//////////////////////////////////////////////////////////////////////////////////// 
void TriggerMetric::PrintMetric(TObject *aFunc, const double aTimeStart, const double aTimeEnd, const string aOutFile){
////////////////////////////////////////////////////////////////////////////////////

  TGraph *Gtmp=NULL;
  TF1 *Ftmp=NULL;

  if(!strcmp(aFunc->ClassName(),"TF1"))
    Ftmp=(TF1*)aFunc;
  else if(!strcmp(aFunc->ClassName(),"TGraph")){
    Gtmp=(TGraph*)aFunc;
    Gtmp->SetBit(TGraph::kIsSortedX);
  }
  else{
    cerr<<"TriggerMetric::PrintMetric: only TGraph or TF1 objects are supported"<<endl;
    return;
  }

  // set time range
  EventMap::SetMapTimeRange(0,aTimeEnd-aTimeStart+1.0);

  // build map
  double tcenter=aTimeStart/2.0+aTimeEnd/2.0;
  EventMap::BuildMap(0,tcenter);
  EventMap::PrintMap(0);

  // frequency range
  double f1;
  double f2;
  if(Ftmp!=NULL){
    f1=Ftmp->GetX(aTimeStart, GetFrequencyMin(), GetFrequencyMax());
    f2=Ftmp->GetX(aTimeEnd-0.001, GetFrequencyMin(), GetFrequencyMax());
  }
  else{
    gsl_interp_accel *acc = gsl_interp_accel_alloc ();
    gsl_spline *interp = gsl_spline_alloc(gsl_interp_linear, Gtmp->GetN());
    gsl_spline_init(interp, Gtmp->GetY(), Gtmp->GetX(), Gtmp->GetN());
    double t1, t2;
    if(aTimeStart<Gtmp->GetY()[0]) t1=Gtmp->GetY()[0];
    else if(aTimeStart>Gtmp->GetY()[Gtmp->GetN()-1]) t1=Gtmp->GetY()[Gtmp->GetN()-1];
    else t1=aTimeStart;
    if(aTimeEnd<Gtmp->GetY()[0]) t2=Gtmp->GetY()[0];
    else if(aTimeEnd>Gtmp->GetY()[Gtmp->GetN()-1]) t2=Gtmp->GetY()[Gtmp->GetN()-1];
    else t2=aTimeEnd;
    f1=gsl_spline_eval(interp, t1, acc);
    f2=gsl_spline_eval(interp, t2, acc);
    gsl_spline_free(interp);
    gsl_interp_accel_free(acc);
  }
  
  // make graph
  double p_df=fabs(f1-f2)/1000.0;
  if(f1>f2) p_df=-p_df;
  TGraph *G = new TGraph(1000);
  if(Ftmp!=NULL){
    for(int p=0; p<1000; p++)
      G->SetPoint(p, Ftmp->Eval(f1+p*p_df)-tcenter, f1+p*p_df);
  }
  else{
    for(int p=0; p<1000; p++)
      G->SetPoint(p, Gtmp->Eval(f1+p*p_df)-tcenter, f1+p*p_df);
  }
  EventMap::Draw(G,"PLsame");

  // save map
  EventMap::Print(aOutFile);
  delete G;
 
  return;
}
