/**
 * @file 
 * @brief Generic C utility functions.
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#ifndef __CUtils__
#define __CUtils__

#include "Config.h"

#define LN2 0.6931471805599453094172321214581766  /**< \f$\ln(2)\f$ */
#define LN10 2.30258509299404590109361379290930926799774169921875  /**< \f$\ln(10)\f$ */
#define SQRT2 1.4142135623730951454746218587388284504413604736328125 /**< \f$\sqrt{2}\f$ */
#define PIOVERTWO 1.5707963267948965579989817342720925807952880859375 /**< \f$\pi/2\f$ */

using namespace std;

/**
 * @brief Utility string stream.
 */
static stringstream gwl_ss;

/**
 * @brief Prints the version.
 * @details This function also checks the GWOLLUM environment:
 * - A temp directory must esist.
 * - `$GWOLLUM_DATA` must point to an existing directory.
 * - `$GWOLLUM_PIX` must point to an existing directory.
 *
 * @returns 0 if the environment is correctly set.
 */
int GwlPrintVersion(void);

/**
 * @brief Tests if an integer is a power of 2.
 * @returns true if this is the case.
 * @param[in] x Integer to test.
 */
inline bool IsPowerOfTwo(unsigned int x){
  return ((x != 0) && !(x & (x - 1)));
};

/**
 * @brief Returns the next power of 2.
 * @param[in] x Value from which to get the next power of 2.
 */
inline unsigned int NextPowerOfTwo(double x){ 
  return (unsigned int)pow(2.0, ceil(log(x)/log(2.0)));
};

/**
 * @brief Returns the bias factor when computing a median with a finite number of samples.
 * @details The bias factor is (see Eq. B12 in <a href="http://arxiv.org/abs/gr-qc/0509116">gr-qc/0509116</a>):
 * \f[
 \alpha = \sum_{l=1}^{n}{\frac{(-1)^{l+1}}{l}}.
 * \f]
 * @param[in] nn Number of samples \f$n\f$.
 */
double medianbiasfactor(const unsigned int nn);

/**
 * @brief Returns the median value of an array of doubles.
 * @details The input vector is sorted. Then the median value is returned.
 * @param[in] aSize Array size.
 * @param[in,out] aVect Pointer to the input array. The array values are sorted after calling this function.
 * @pre The size of the input vector is expected to be non 0.
 */
double GetMedian(const unsigned int aSize, double *aVect);

/**
 * @brief Splits a string into sub-strings.
 * @details A string is divided into an array of words.
 * The separator defines where to break the string.
 * The separator should be a single character.
 * @returns A vector of string.
 * @param[in] stringtodivide String to divide.
 * @param[in] separator Separator.
 */
vector<string> SplitString(const string stringtodivide, const char separator=' ');

/**
 * @brief Transforms a string to uppercase.
 * @param[in] stringtoconvert String to transform.
 */
string StringToUpper(string stringtoconvert);

/**
 * @brief Returns the file name given the file path.
 * @details For example "myprog.exe" is returned from:
 * - the full path "/usr/bin/myprog.exe"
 * - the relative path "./bin/myprog.exe"
 * @param[in] filepathname File path.
 */
inline string GetFileNameFromPath(const string filepathname){
  return filepathname.substr(filepathname.find_last_of("/")+1);
};

/**
 * @brief Replaces all occurences of a sub-string by another sub-string.
 * @details All occurences of substring 'from' are replaced by 'to'.
 * @returns The modified string is returned.
 * @param[in] str String to modify.
 * @param[in] from Substring to replace from.
 * @param[in] to Substring to replace to.
 */
string ReplaceAll(string str, const string& from, const string& to);

/**
 * @brief Returns a list of sub-directories.
 * @details The vector 'subdir' is filled with directories contained in 'maindir'
 * @returns false if the main directory does not exist.
 * @param[out] subdir List of sub-directories.
 * @param[in] maindir Path to the main directory.
 */
bool ListDirectories(vector <string> &subdir, const string maindir);

/**
 * @brief C utility to extract a list of files.
 * @returns A list of files matching a pattern.
 * @param[in] pattern File pattern.
 */
vector<string> Glob(const char* pattern);

/**
 * @brief Returns a Tukey window.
 * @param[in] aSize Window size.
 * @param[in] aAlpha Total fraction of the window used to transition from 0 to 1 and 1 to 0. =0: rectangle, =1: HAnn window.
 */
double* GetTukeyWindow(const unsigned int aSize, const double aAlpha);

/**
 * @brief C utility to generate a WAV file.
 * @param[in] aFileName Output .wav file name.
 * @param[in] aN Number of samples.
 * @param[in] aSamplingRate Sampling rate [Hz].
 * @param[in] aAmplitudeLeft Amplitudes for left audio.
 * @param[in] aAmplitudeRight Amplitudes for right audio.
 * @param[in] aScale Scale factor ("volume") applied to amplitudes.
 * @param[in] aOffset Offset applied to the number of samples.
 * @pre The size of the amplitude array should match the number of samples.
 */
void MakeStereoSoundFile(const string aFileName, const unsigned int aN, const unsigned int aSamplingRate,
			 double *aAmplitudeLeft, double *aAmplitudeRight,
			 const double aScale=1.0, const int aOffset=0);

#endif


