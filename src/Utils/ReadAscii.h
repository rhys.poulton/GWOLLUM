/**
 * @file 
 * @brief Parser for text files with columns.
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#ifndef __ReadAscii__
#define __ReadAscii__

#include "CUtils.h"

#define NCOLMAX 50

using namespace std;

/**
 * @brief Parse text files with columns.
 * @details Many operations are then available to access and modify the data elements.
 * @author Florent Robinet
 */
class ReadAscii{

 public:
  
  /**
   * @name Constructors and destructors
   @{
  */

  /**
   * @brief Constructor of the ReadAscii class.
   * @details With this constructor, the text file is loaded and data are extracted.
   * - The text file must be composed of a fix number of data columns.
   * - This file should not contain more than ::NCOLMAX columns.
   * - Columns should be separated by white spaces.
   * - Empty lines and commented lines ('//', '#' or '\%') are ignored.
   * - If known, the format of each column should be provided in argument.
   * The format syntax is the folowing: \"t;t;t\", where ';' separates each column definition and 't' refers to the type of data which is expected.
   * Three data types are supported:
   *   - 's' for a string type
   *   - 'i' for a int type
   *   - 'u' for an unsigned int type
   *   - 'd' for a double/float type
   *
   * \code{.cpp}
   * ReadAscii("/path/to/my/file.txt", "s;s;i;i");
   * \endcode
   *
   * This example means that the text file is composed of 4 columns, the 2 first ones are string fields and the 2 last ones are integer fields.
   *
   * If the input format is unknown, 'aformat' should be set to "UNKNOWN" which it is by default.
   * In that case, all columns are assumed to have a string format.
   * This default format can be changed later on by using the SetFormat() function.
   *
   * Lines with a wrong number of columns (as defined by aformat or by the first line) are skipped.
   * @param[in] afilename Path to the text file.
   * @param[in] aformat String format to be expected in the file.
   */
  ReadAscii(const string afilename, const string aformat="UNKNOWN");

  /**
   * @brief Destructor of the ReadAscii class.
   */
  virtual ~ReadAscii(void);
 
  /**
     @}
  */

  /**
   * @brief Returns the number of rows/lines.
   */
  inline unsigned int GetNLine(void){ return mylines.size(); };

  /**
   * @brief Returns the number of rows/lines.
   */
  inline unsigned int GetNRow(void){ return mylines.size(); };

  /**
   * @brief Returns the number of columns.
   */
  inline unsigned int GetNCol(void){ return Vformat.size(); };

  /**
   * @brief Defines a new column format ('s', 'd', 'u' or 'i').
   * @details The selected column is converted to a new format.
   * Only one conversion is destructive: 'd' to 'i/u'.
   * In that case, the doubles are recasted as integers.
   * true is returned in case of success.
   * @param[in] co Selected column number (indexing starts at 0).
   * @param[in] aformat char new format ('s', 'd'. 'u' or 'i').
   */
  bool SetFormat(const unsigned int co, const char aformat);

  /**
   * @brief Returns the content of a line as a string.
   * @details true is returned in case of success.
   * @param[out] line Returned line.
   * @param[in] li Selected line number (indexing starts at 0).
   */
  bool GetLine(string &line, const unsigned int li);

  /**
   * @brief Returns the content of an integer column in a vector.
   * @details true is returned in case of success.
   * @param[out] col Returned vector of integer data.
   * @param[in] co Selected column number (indexing starts at 0).
   */
  bool GetCol(vector<int> &col, const unsigned int co);

  /**
   * @brief Returns the content of an unsigned integer column in a vector.
   * @details true is returned in case of success.
   * @param[out] col Returned vector of integer data.
   * @param[in] co Selected column number (indexing starts at 0).
   */
  bool GetCol(vector<unsigned int> &col, const unsigned int co);

  /**
   * @brief Returns the content of a double column in a vector.
   * @details true is returned in case of success.
   * @param[out] col Returned vector of double data.
   * @param[in] co Selected column number (indexing starts at 0).
   */
  bool GetCol(vector<double> &col, const unsigned int co);

  /**
   * @brief Returns the content of a string column in a vector.
   * @details true is returned in case of success.
   * @param[out] col Returned vector of string data.
   * @param[in] co Selected column number (indexing starts at 0).
   */
  bool GetCol(vector<string> &col, const unsigned int co);

  /**
   * @brief Returns the integer data value at a given line and column position.
   * @details true is returned in case of success.
   * @param[out] element Returned data value.
   * @param[in] co Selected column number (indexing starts at 0).
   * @param[in] li Selected line number (indexing starts at 0).
   */
  bool GetElement(int &element, const unsigned int li, const unsigned int co);

  /**
   * @brief Returns the unsigned integer data value at a given line and column position.
   * @details true is returned in case of success.
   * @param[out] element Returned data value.
   * @param[in] co Selected column number (indexing starts at 0).
   * @param[in] li Selected line number (indexing starts at 0).
   */
  bool GetElement(unsigned int &element, const unsigned int li, const unsigned int co);

  /**
   * @brief Returns the double data value at a given line and column position.
   * @details true is returned in case of success.
   * @param[out] element Returned data value.
   * @param[in] co Selected column number (indexing starts at 0).
   * @param[in] li Selected line number (indexing starts at 0).
   */
  bool GetElement(double &element, const unsigned int li, const unsigned int co);

  /**
   * @brief Returns the string data value at a given line and column position.
   * @details true is returned in case of success.
   * @param[out] element Returned data value.
   * @param[in] co Selected column number (indexing starts at 0).
   * @param[in] li Selected line number (indexing starts at 0).
   */
  bool GetElement(string &element, const unsigned int li, const unsigned int co);

 private:

  // extract the elements of the files
  void ExtractFormat(void); ///< Extract format vector.
  void ExtractLines(void);  ///< Extract lines.
  void ExtractCol(void);    ///< Extract columns.
  void GetMinMax(const unsigned int co); ///< Extract min/max values.

  string ffilename;           ///< Input file name.
  string fformat;             ///< Input format string.
  vector<string> Vformat;     ///< Vector of formats.
  vector <string> mylines;    ///< Lines of the files.
  vector <string> scol[NCOLMAX]; ///< Column of string.
  vector <double> dcol[NCOLMAX]; ///< Column of double.
  vector <int>    icol[NCOLMAX]; ///< Column of int.
  vector <unsigned int> ucol[NCOLMAX]; ///< Column of unsigned int.
  double colmin[NCOLMAX];        ///< Minimum in a given column.
  double colmax[NCOLMAX];        ///< Maximum in a given column.
};
 
#endif









