/**
 * @file 
 * @brief See ReadAscii.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "ReadAscii.h"

/////////////////////////////////////////////////////////////////////
ReadAscii::ReadAscii(const string afilename, const string aformat){
/////////////////////////////////////////////////////////////////////
    
  // arguments
  ffilename=afilename;
  fformat=aformat;

  // extract the file format if given
  if(fformat.compare("UNKNOWN")) ExtractFormat();

  if(!filesystem::is_regular_file(ffilename))
    cerr<<"ReadAscii::ReadAscii: The input text file "<<afilename<<" cannot be read"<<endl; 
  else ExtractLines();
    
  // extract the file columns
  ExtractCol();
  
  // get min/max
  for(unsigned int co=0; co<Vformat.size(); co++) GetMinMax(co);
  
}

/////////////////////////////////////////////////////////////////////
ReadAscii::~ReadAscii(){
/////////////////////////////////////////////////////////////////////
  mylines.clear();
  Vformat.clear();
  for(unsigned int co=0; co<NCOLMAX; co++){
    scol[co].clear();
    dcol[co].clear();
    icol[co].clear();
    ucol[co].clear();
  }
}

/////////////////////////////////////////////////////////////////////
void ReadAscii::ExtractFormat(void){
/////////////////////////////////////////////////////////////////////
  
  // get the format of each column
  Vformat = SplitString(fformat, ';');
  
  // number of columns
  if(Vformat.size()<1 || Vformat.size()>NCOLMAX){
    cerr<<"ReadAscii::ExtractFormat: the input format "<<fformat<<" contains too few or too many columns"<<endl; 
    Vformat.clear();
  }

  // check requested format
  for(unsigned int co=0; co<Vformat.size(); co++){
    if( !Vformat[co].compare("d") && 
	!Vformat[co].compare("i") && 
	!Vformat[co].compare("s") ){
      cerr<<"ReadAscii::ExtractFormat: the input format "<<Vformat[co]<<" is unknown (d/i/s)"<<endl; 
      Vformat.clear();
    }
  }

  return;
}

/////////////////////////////////////////////////////////////////////
void ReadAscii::ExtractLines(void){
/////////////////////////////////////////////////////////////////////
  
  ifstream myfile(ffilename.c_str());
  string curline;// current line
  vector <string> linewords;

  if (myfile.is_open()){
    while ( myfile.good() ){
      getline(myfile, curline);
      if(curline.empty()) continue; //remove empty lines
      curline.erase(0,curline.find_first_not_of(' '));// remove white spaces
      if(!curline.compare(0,2,"//")) continue; //remove commented lines
      if(!curline.compare(0,1,"#")) continue; //remove commented lines
      if(!curline.compare(0,1,"%")) continue; //remove commented lines
      

      if(!fformat.compare("UNKNOWN")){ // unknown format --> check first line format
	if(mylines.size()==0){
	  linewords=SplitString(curline, ' ');
	  fformat.clear(); fformat="";
	  for(unsigned int f=0; f<linewords.size(); f++) fformat+="s;";
	  ExtractFormat();
	}
      }
      else{ // check the line number of columns
	linewords=SplitString(curline, ' ');
	if(linewords.size()!=Vformat.size()) continue;
      }

      // save line
      mylines.push_back(curline);
      linewords.clear();
    }
  }

  myfile.close();
  return;
}

/////////////////////////////////////////////////////////////////////
void ReadAscii::ExtractCol(void){
/////////////////////////////////////////////////////////////////////

  vector<string> elements;
  double tmp_d; int tmp_i;
  unsigned int tmp_u;

  // loop over lines
  for(unsigned int li=0; li<mylines.size(); li++){

    //extract line elements as a string
    elements.clear();
    elements = SplitString(mylines[li], ' ');
  
    //convert string into appropriate format
    for(unsigned int co=0; co<Vformat.size(); co++){
      istringstream iss(elements[co]);
      if(!Vformat[co].compare("d")){ // double
	iss>>tmp_d; dcol[co].push_back(tmp_d);
      }
      else if(!Vformat[co].compare("i")){ // int
	iss>>tmp_i; icol[co].push_back(tmp_i);
      }
      else if(!Vformat[co].compare("u")){ // int
	iss>>tmp_u; ucol[co].push_back(tmp_u);
      }
      else{// string
	scol[co].push_back(elements[co]);
      }
    }
    
  }
  
  return;
}

/////////////////////////////////////////////////////////////////////
void ReadAscii::GetMinMax(const unsigned int co){
/////////////////////////////////////////////////////////////////////
  if(!Vformat[co].compare("s")) return;
  colmin[co]=1.0e100;
  colmax[co]=-1.0e100;
    
  for(unsigned int li=0; li<mylines.size(); li++){
    if(!Vformat[co].compare("i")){
      if((double)icol[co][li]<=colmin[co]||!li) colmin[co]=icol[co][li];
      if((double)icol[co][li]>=colmax[co]||!li) colmax[co]=icol[co][li];
    }
    else if(!Vformat[co].compare("u")){
      if((double)ucol[co][li]<=colmin[co]||!li) colmin[co]=ucol[co][li];
      if((double)ucol[co][li]>=colmax[co]||!li) colmax[co]=ucol[co][li];
    }
    else{
      if(dcol[co][li]<=colmin[co]||!li) colmin[co]=dcol[co][li];
      if(dcol[co][li]>=colmax[co]||!li) colmax[co]=dcol[co][li];
    }
  }

  return;
}

/////////////////////////////////////////////////////////////////////
bool ReadAscii::SetFormat(const unsigned int co, const char aformat){
/////////////////////////////////////////////////////////////////////
  if(co>=Vformat.size()){
    cerr<<"ReadAscii::SetFormat: column #"<<co<<" does not exist"<<endl; 
    return false;
  }

  string sformat(1, aformat);
  if(sformat.compare("i")&&sformat.compare("d")&&sformat.compare("u")&&sformat.compare("s")){
    cerr<<"ReadAscii::SetFormat: The format "<<sformat<<" is unknown (d/i/u/s)"<<endl; 
    return false;
  }

  // no update is necessary
  if(!Vformat[co].compare(sformat)) return true;
  
  // update format
  string format_old=Vformat[co];
  Vformat[co]=sformat;

  double tmp_d; int tmp_i; unsigned int tmp_u;

  // convert column s
  if(!format_old.compare("s")){// from string
    dcol[co].clear();
    icol[co].clear();
    ucol[co].clear();
    for(unsigned int li=0; li<mylines.size(); li++){
      istringstream iss(scol[co][li]);
      if(!Vformat[co].compare("d")){ // double
	iss>>tmp_d; dcol[co].push_back(tmp_d);
      }
      else if(!Vformat[co].compare("u")){ // unsigned
	iss>>tmp_u; ucol[co].push_back(tmp_u);
      }
      else{ // int
	iss>>tmp_i; icol[co].push_back(tmp_i);
      }
    }
    scol[co].clear();
    GetMinMax(co);
    return true;
  }

  // convert column i/u/d
  else{
    scol[co].clear();
    if(!Vformat[co].compare("d")) dcol[co].clear();
    else if(!Vformat[co].compare("u")) ucol[co].clear();
    else                          icol[co].clear();
    ostringstream oss;
    for(unsigned int li=0; li<mylines.size(); li++){
      if(!Vformat[co].compare("d")&&!format_old.compare("i")) // int to double
	dcol[co].push_back((double)icol[co][li]);
      else if(!Vformat[co].compare("d")&&!format_old.compare("u")) // unsigned int to double
	dcol[co].push_back((double)ucol[co][li]);
      else if(!Vformat[co].compare("i")&&!format_old.compare("u")) // unsigned int to int
	icol[co].push_back((int)ucol[co][li]);
      else if(!Vformat[co].compare("i")&&!format_old.compare("d")) // double to int
	icol[co].push_back((int)dcol[co][li]);
      else if(!Vformat[co].compare("u")&&!format_old.compare("d")) // double to unsigned int
	ucol[co].push_back((unsigned int)dcol[co][li]);
      else if(!Vformat[co].compare("u")&&!format_old.compare("i")) // int to unsigned int
	ucol[co].push_back((unsigned int)icol[co][li]);
      else{ // to string
	if(!Vformat[co].compare("d")) oss<<dcol[co][li];
	else if(!Vformat[co].compare("u")) oss<<ucol[co][li];
	else                          oss<<icol[co][li];
	scol[co].push_back(oss.str());
	oss.str(""); oss.clear();
      }
    }
    if(!Vformat[co].compare("d")){
      icol[co].clear(); ucol[co].clear();
    }
    else if(!Vformat[co].compare("u")){
      icol[co].clear(); dcol[co].clear(); 
    }
    else{
      dcol[co].clear(); ucol[co].clear();
    }
    GetMinMax(co);
    return true;
  }
  
  return false;
}


/////////////////////////////////////////////////////////////////////
bool ReadAscii::GetLine(string &line, const unsigned int li){
/////////////////////////////////////////////////////////////////////
  if(li>=GetNRow()){
    cerr<<"ReadAscii::GetLine: line #"<<li<<" does not exist"<<endl; 
    return false;
  }
  line = mylines[li];
  return true;
}

/////////////////////////////////////////////////////////////////////
bool ReadAscii::GetCol(vector<int> &col, const unsigned int co){
/////////////////////////////////////////////////////////////////////
  if(co>=Vformat.size()){
    cerr<<"ReadAscii::GetCol: column #"<<co<<" does not exist"<<endl; 
    return false;
  }
  if(Vformat[co].compare("i")){
    cerr<<"ReadAscii::GetCol: column #"<<co<<" does not contain integers but "<<Vformat[co]<<endl; 
    return false;
  }
  col.clear();
  col = icol[co];
  return true;
}

/////////////////////////////////////////////////////////////////////
bool ReadAscii::GetCol(vector<unsigned int> &col, const unsigned int co){
/////////////////////////////////////////////////////////////////////
  if(co>=Vformat.size()){
    cerr<<"ReadAscii::GetCol: column #"<<co<<" does not exist"<<endl; 
    return false;
  }
  if(Vformat[co].compare("u")){
    cerr<<"ReadAscii::GetCol: column #"<<co<<" does not contain unsigned integers but "<<Vformat[co]<<endl; 
    return false;
  }
  col.clear();
  col = ucol[co];
  return true;
}

/////////////////////////////////////////////////////////////////////
bool ReadAscii::GetCol(vector<double> &col, const unsigned int co){
/////////////////////////////////////////////////////////////////////
  if(co>=Vformat.size()){
    cerr<<"ReadAscii::GetCol: column #"<<co<<" does not exist"<<endl; 
    return false;
  }
  if(Vformat[co].compare("d")){
    cerr<<"ReadAscii::GetCol: column #"<<co<<" does not contain doubles but "<<Vformat[co]<<endl; 
    return false;
  }
  col.clear();
  col = dcol[co];
  return true;
}

/////////////////////////////////////////////////////////////////////
bool ReadAscii::GetCol(vector<string> &col, const unsigned int co){
/////////////////////////////////////////////////////////////////////
  if(co>=Vformat.size()){
    cerr<<"ReadAscii::GetCol: column #"<<co<<" does not exist"<<endl; 
    return false;
  }
  if(Vformat[co].compare("s")){
    cerr<<"ReadAscii::GetCol: column #"<<co<<" does not contain strings but"<<Vformat[co]<<endl; 
    return false;
  }
  col.clear();
  col = scol[co];
  return true;
}

/////////////////////////////////////////////////////////////////////
bool ReadAscii::GetElement(int &element, const unsigned int li, const unsigned int co){
/////////////////////////////////////////////////////////////////////
  if(li>=GetNRow()){
    cerr<<"ReadAscii::GetElement: line #"<<li<<" does not exist"<<endl; 
    return false;
  }
  if(co>=Vformat.size()){
    cerr<<"ReadAscii::GetElement: column #"<<co<<" does not exist"<<endl; 
    return false;
  }
  if(Vformat[co].compare("i")){
    cerr<<"ReadAscii::GetElement: column #"<<co<<" does not contain integers but "<<Vformat[co]<<endl; 
    return false;
  }
  element=icol[co][li];
  return true;
}

/////////////////////////////////////////////////////////////////////
bool ReadAscii::GetElement(unsigned int &element, const unsigned int li, const unsigned int co){
/////////////////////////////////////////////////////////////////////
  if(li>=GetNRow()){
    cerr<<"ReadAscii::GetElement: line #"<<li<<" does not exist"<<endl; 
    return false;
  }
  if(co>=Vformat.size()){
    cerr<<"ReadAscii::GetElement: column #"<<co<<" does not exist"<<endl; 
    return false;
  }
  if(Vformat[co].compare("u")){
    cerr<<"ReadAscii::GetElement: column #"<<co<<" does not contain unsigned integers but "<<Vformat[co]<<endl; 
    return false;
  }
  element=ucol[co][li];
  return true;
}

/////////////////////////////////////////////////////////////////////
bool ReadAscii::GetElement(double &element, const unsigned int li, const unsigned int co){
/////////////////////////////////////////////////////////////////////
  if(li>=GetNRow()){
    cerr<<"ReadAscii::GetElement: line #"<<li<<" does not exist"<<endl; 
    return false;
  }
  if(co>=Vformat.size()){
    cerr<<"ReadAscii::GetElement: column #"<<co<<" does not exist"<<endl; 
    return false;
  }
  if(Vformat[co].compare("d")){
    cerr<<"ReadAscii::GetElement: column #"<<co<<" does not contain doubles but "<<Vformat[co]<<endl; 
    return false;
  }
  element=dcol[co][li];
  return true;
}

/////////////////////////////////////////////////////////////////////
bool ReadAscii::GetElement(string &element, const unsigned int li, const unsigned int co){
/////////////////////////////////////////////////////////////////////
  if(li>=GetNRow()){
    cerr<<"ReadAscii::GetElement: line #"<<li<<" does not exist"<<endl; 
    return false;
  }
  if(co>=Vformat.size()){
    cerr<<"ReadAscii::GetElement: column #"<<co<<" does not exist"<<endl; 
    return false;
  }
  if(Vformat[co].compare("s")){
    cerr<<"ReadAscii::GetElement: column #"<<co<<" does not contain strings but "<<Vformat[co]<<endl; 
    return false;
  }
  element=scol[co][li];
  return true;
}


