/**
 * @file 
 * @brief See GwollumPlot.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "GwollumPlot.h"

ClassImp(GwollumPlot)

////////////////////////////////////////////////////////////////////////////////////
GwollumPlot::GwollumPlot(const string aName, const string aStyleName){
////////////////////////////////////////////////////////////////////////////////////
  
  // random id
  srand(time(NULL));
  randid = rand();
  gwl_ss<<randid;
  srandid = gwl_ss.str();
  gwl_ss.clear(); gwl_ss.str("");
  name = aName;
  
  // set style
  if(!aStyleName.compare("GWOLLUM")) SetGwollumStyle();
  else if(!aStyleName.compare("FIRE")) SetFireStyle();
  else if(!aStyleName.compare("PINK")) SetPinkStyle();
  else if(!aStyleName.compare("STANDARD")) SetStandardStyle();
  else{
    cerr<<"GwollumPlot::GwollumPlot: "+aStyleName+" style is not supported"<<endl;
    cerr<<"GwollumPlot::GwollumPlot: use GWOLLUM style by default"<<endl;
    SetGwollumStyle();
  }

  // create working canvas and pad
  Wcan = new TCanvas((name+"_can_"+srandid).c_str(), (name+"_can").c_str(),0,0,850,500);
  Wcan->Draw();
  Wpad = new TPad((name+"_pad_"+srandid).c_str(), (name+"_pad").c_str(),0,0,1,1);
  Wpad->Draw();
  Wpad->cd();
   
  // legends
  Wleg = new TLegend(0.8, 0.1, 0.98, 0.9);
  Wleg->SetName((name+"_leg").c_str());
  Wleg->SetTextColor(gStyle->GetTextColor());// this is needed. bug in root?

  // additional text
  Wtext = new TText();
  Wtext->SetNDC(true);
}

////////////////////////////////////////////////////////////////////////////////////
GwollumPlot::~GwollumPlot(void){
////////////////////////////////////////////////////////////////////////////////////
  delete Wtext;
  delete Wpad;
  delete Wcan;
  delete Wleg;
}

////////////////////////////////////////////////////////////////////////////////////
void GwollumPlot::AddText(const string aText,
                          const double aX, const double aY, const double aSize,
                          const int aPadIndex){
////////////////////////////////////////////////////////////////////////////////////
  Wpad->cd(aPadIndex); 
  Wtext->SetText(aX, aY, aText.c_str());
  Wtext->SetTextSize(aSize);
  Wtext->Draw("same");
  gPad->Modified(); 
  gPad->Update();
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void GwollumPlot::UpdateText(const string aText){
////////////////////////////////////////////////////////////////////////////////////
  Wtext->SetText(Wtext->GetX(), Wtext->GetY(), aText.c_str());
  Wcan->Modified();
  Wcan->Update();
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void GwollumPlot::AddLegendEntry(const TObject *aObj, const string aLabel, const string aStyle){
////////////////////////////////////////////////////////////////////////////////////
  Wleg->AddEntry(aObj,aLabel.c_str(),aStyle.c_str());
  Wcan->Modified();
  Wcan->Update();
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void GwollumPlot::AddLegendHeader(const string aLabel){
////////////////////////////////////////////////////////////////////////////////////
  Wleg->SetHeader(aLabel.c_str());
  Wcan->Modified();
  Wcan->Update();
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void GwollumPlot::DrawLegend(void){
////////////////////////////////////////////////////////////////////////////////////
  Wcan->cd(); 
  Wpad->SetPad(0.0, 0.0, 0.94, 1.0);
  Wpad->Modified(); gPad->Update();
  Wleg->Draw("same");
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void GwollumPlot::UnDrawLegend(void){
////////////////////////////////////////////////////////////////////////////////////
  Wcan->cd();
  Wpad->GetListOfPrimitives()->Remove(Wleg);
  Wcan->GetListOfPrimitives()->Remove(Wleg);
  Wpad->SetPad(0.0, 0.0, 1.0, 1.0);
  Wpad->Modified(); gPad->Update();
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void GwollumPlot::Print(const string aFileName, const double aScaleFactor){
////////////////////////////////////////////////////////////////////////////////////
  if(aScaleFactor<=0) return;
  Wcan->cd(); 
  unsigned int wdth = GetWidth();
  unsigned int hght = GetHeight();
  unsigned int newWidth=(unsigned int)ceil(aScaleFactor*wdth);
  unsigned int newHeight=(unsigned int)ceil(aScaleFactor*hght);
  ResizePlot(newWidth, newHeight);
  Print(aFileName.c_str());
  return ResizePlot(wdth, hght);
}

////////////////////////////////////////////////////////////////////////////////////
void GwollumPlot::Print(const string aFileName, const unsigned int aNewWidth, const unsigned int aNewHeight){
////////////////////////////////////////////////////////////////////////////////////
  Wcan->cd(); 
  unsigned int wdth = GetWidth();
  unsigned int hght = GetHeight();
  ResizePlot(aNewWidth, aNewHeight);
  Print(aFileName.c_str());
  return ResizePlot(wdth, hght);
}

////////////////////////////////////////////////////////////////////////////////////
void GwollumPlot::SetGwollumStyle(void){
////////////////////////////////////////////////////////////////////////////////////

  // General
  stylename="GWOLLUM";
  gStyle->SetFillColor(1);
  gStyle->SetCanvasColor(1);
  gStyle->SetPadColor(1);
  gStyle->SetPadLeftMargin(0.12);
  gStyle->SetPadRightMargin(0.16);
  gStyle->SetMarkerColor(7);
  gStyle->SetLineColor(7);
  gStyle->SetFuncColor(7);
  gStyle->SetTitleBorderSize(0);

  // Frame
  gStyle->SetFrameFillColor(1);
  gStyle->SetFrameLineColor(0);
  gStyle->SetLegendBorderSize(0);
  gStyle->SetLegendFillColor(1);
  gStyle->SetStatColor(1);
  gStyle->SetStatTextColor(0);

  // axis
  gStyle->SetPalette(51);
  gStyle->SetNumberContours(104);
  gStyle->SetAxisColor(0,"XYZ");
  gStyle->SetLabelColor(0,"XYZ");
  gStyle->SetTitleColor(0,"XYZ");
  gStyle->SetGridColor(0);

  // histos
  gStyle->SetOptStat(0);
  gStyle->SetTitleTextColor(0);
  gStyle->SetTitleFillColor(1);
  gStyle->SetHistLineColor(7);
  gStyle->SetHistLineWidth(2);
  //gStyle->SetHistFillColor(0);

  // text
  gStyle->SetTextColor(0);
}

////////////////////////////////////////////////////////////////////////////////////
void GwollumPlot::SetPinkStyle(void){
////////////////////////////////////////////////////////////////////////////////////

  // General
  stylename="PINK";
  gStyle->SetFillColor(1);
  gStyle->SetCanvasColor(1);
  gStyle->SetPadColor(1);
  gStyle->SetPadLeftMargin(0.12);
  gStyle->SetPadRightMargin(0.16);
  gStyle->SetMarkerColor(6);
  gStyle->SetLineColor(6);
  gStyle->SetFuncColor(6);
  gStyle->SetTitleBorderSize(0);
 
  // Frame
  gStyle->SetFrameFillColor(1);
  gStyle->SetFrameLineColor(0);
  gStyle->SetLegendBorderSize(0);
  gStyle->SetLegendFillColor(1);
  gStyle->SetStatColor(1);
  gStyle->SetStatTextColor(0);

  // axis
  gStyle->SetPalette(81);
  gStyle->SetNumberContours(104);
  gStyle->SetAxisColor(0,"XYZ");
  gStyle->SetAxisColor(1,"Z");
  gStyle->SetLabelColor(0,"XYZ");
  gStyle->SetTitleColor(0,"XYZ");
  gStyle->SetGridColor(0);

  // histos
  gStyle->SetOptStat(0);
  gStyle->SetTitleTextColor(0);
  gStyle->SetTitleFillColor(1);
  gStyle->SetHistLineColor(6);
  gStyle->SetHistLineWidth(2);

  // text
  gStyle->SetTextColor(0);
}

////////////////////////////////////////////////////////////////////////////////////
void GwollumPlot::SetFireStyle(void){
////////////////////////////////////////////////////////////////////////////////////

  // General
  stylename="FIRE";
  gStyle->SetFillColor(1);
  gStyle->SetCanvasColor(1);
  gStyle->SetPadColor(1);
  gStyle->SetPadLeftMargin(0.12);
  gStyle->SetPadRightMargin(0.16);
  gStyle->SetMarkerColor(5);
  gStyle->SetLineColor(5);
  gStyle->SetFuncColor(5);
  gStyle->SetTitleBorderSize(0);
 
  // Frame
  gStyle->SetFrameFillColor(1);
  gStyle->SetFrameLineColor(0);
  gStyle->SetLegendBorderSize(0);
  gStyle->SetLegendFillColor(1);
  gStyle->SetStatColor(1);
  gStyle->SetStatTextColor(0);

  // axis
  gStyle->SetPalette(53);
  gStyle->SetNumberContours(104);
  gStyle->SetAxisColor(0,"XYZ");
  gStyle->SetAxisColor(1,"Z");
  gStyle->SetLabelColor(0,"XYZ");
  gStyle->SetTitleColor(0,"XYZ");
  gStyle->SetGridColor(0);

  // histos
  gStyle->SetOptStat(0);
  gStyle->SetTitleTextColor(0);
  gStyle->SetTitleFillColor(1);
  gStyle->SetHistLineColor(5);
  gStyle->SetHistLineWidth(2);

  // text
  gStyle->SetTextColor(0);
}

////////////////////////////////////////////////////////////////////////////////////
void GwollumPlot::SetStandardStyle(void){
////////////////////////////////////////////////////////////////////////////////////
  
  // General
  stylename="STANDARD";
  gStyle->SetFillColor(0);
  gStyle->SetCanvasColor(0);
  gStyle->SetPadColor(0);
  gStyle->SetPadLeftMargin(0.12);
  gStyle->SetPadRightMargin(0.16);
  gStyle->SetMarkerColor(kBlue+2);
  gStyle->SetLineColor(kBlue+2);
  gStyle->SetFuncColor(kBlue+2);
  gStyle->SetTitleBorderSize(0);
 
  // Frame
  gStyle->SetFrameFillColor(0);
  gStyle->SetFrameLineColor(1);
  gStyle->SetLegendBorderSize(0);
  gStyle->SetLegendFillColor(0);
  gStyle->SetStatColor(0);
  gStyle->SetStatTextColor(1);

  // axis
  gStyle->SetNumberContours(104);
  gStyle->SetPalette(55);
  gStyle->SetAxisColor(1,"XYZ");
  gStyle->SetLabelColor(1,"XYZ");
  gStyle->SetTitleColor(1,"XYZ");
  gStyle->SetGridColor(1);

  // histos
  gStyle->SetOptStat(0);
  gStyle->SetTitleTextColor(1);
  gStyle->SetTitleFillColor(0);
  gStyle->SetHistLineColor(kBlue+2);
  gStyle->SetHistLineWidth(2);
  
  // text
  gStyle->SetTextColor(1);
}


/*
bool GwollumPlot::PrintLogo(Float_t v_scale, Float_t skew){

  if(gPad==NULL) return false;
  if(logopad!=NULL){ delete logopad; logopad=NULL; }
  if(logo!=NULL){ delete logo; logo=NULL; }

  // logo
  logo = TImage::Open("$GWOLLUM_DOC/Pics/gwollum_logo_min_trans.gif");
  if(!logo){
    logo=NULL;
    return false;
  }

  logo->SetConstRatio(kFALSE);
  UInt_t h_ = logo->GetHeight();
  UInt_t w_ = logo->GetWidth();
  
  Float_t r = w_/h_;
  Wpad->Update();
  Float_t rpad = Double_t(Wpad->VtoAbsPixel(0) - Wpad->VtoAbsPixel(1))/(Wpad->UtoAbsPixel(1) - Wpad->UtoAbsPixel(0));
  r *= rpad;
  
  Float_t d = 0.055;
  // absolute coordinates
  Float_t x1L = 0.01;
  Float_t y1T = 0.99;
  Float_t x1R = x1L + d*r/skew;
  Float_t y1B = y1T - d*v_scale*skew;
    
  logopad = new TPad("logopad", "logopad", x1L, y1B, x1R, y1T );
  logopad->SetRightMargin(0);
  logopad->SetBottomMargin(0);
  logopad->SetLeftMargin(0);
  logopad->SetTopMargin(0);
    
  Int_t xSizeInPixel = logopad->UtoAbsPixel(1) - logopad->UtoAbsPixel(0);
  Int_t ySizeInPixel = logopad->VtoAbsPixel(0) - logopad->VtoAbsPixel(1);
  if (xSizeInPixel<=25 || ySizeInPixel<=25) {
    delete logopad; logopad=NULL;
    return false; // ROOT doesn't draw smaller than this
  }

  logopad->Draw();
  logopad->cd();
  logo->Draw();


  return true;
} 
*/
