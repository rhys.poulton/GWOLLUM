/**
 * @file 
 * @brief Run a coincidence algorithm between two trigger sets.
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#ifndef __Coinc2__
#define __Coinc2__

#include "GwollumPlot.h"
#include "TriggerSelect.h"

using namespace std;

/**
 * @brief Number of trigger sets.
 */
#define COINC2_NT 2

/**
 * @brief Set two trigger sets in coincidence.
 * @details A time coincidence algorithm is implemented in MakeCoinc().
 * The trigger sets must be defined with SetTriggers().
 * This class also offers methods to plot coincident triggers.
 * These plots are generated with MakeComparators().
 * @author Florent Robinet
 */
class Coinc2: public GwollumPlot {

 public:

  /**
   * @name Constructors and destructors
   @{
  */
  /**
   * @brief Constructor of the Coinc2 class.
   * @param[in] aVerbose Verbosity level.
   */
  Coinc2(const unsigned int aVerbose=0);

  /**
   * @brief Destructor of the Coinc2 class.
   */
  virtual ~Coinc2(void);
  /**
     @}
  */

  /**
   * @brief Sets the trigger sets.
   * @details The two input ReadTriggers objects define the trigger sets. Only clusters are used in the coincidence. Therefore, triggers must be clustered first using Triggers::Clusterize().
   *
   * Clusters are selected based on time and frequency: see SelectClusters(). After selection, a cluster is said to be "active".
   * @warning Unactive clusters are tagged with a negative value.
   *
   * Clusters can be time-shifted with a given time offset.
   *
   * In this function, the coinc segments are constructed. The coinc segments are the intersection of:
   * - the input trigger segments after applying the time offsets
   * - the input segments aValidSegments if any.
   *
   * @param[in] aTrigger0 Pointer to the first trigger object (must be a valid object).
   * @param[in] aTrigger1 Pointer to the second trigger object (must be a valid object).
   * @param[in] aTimeOffset0 Time offset to apply to the clusters of the first set.
   * @param[in] aTimeOffset1 Time offset to apply to the clusters of the second set.
   * @param[in] aValidSegments List of segments to consider clusters (before time offset!). Set this to NULL to only use the input trigger segments.
   */
  bool SetTriggers(ReadTriggers *aTrigger0, ReadTriggers *aTrigger1,
                   const double aTimeOffset0=0.0, const double aTimeOffset1=0.0,
                   Segments *aValidSegments=NULL);
  
  /**
   * @brief Runs the coincidence algorithm.
   * @details The trigger sets must first be defined with SetTriggers(). Only active clusters are considered (see SelectClusters()). For each cluster, an effective duration is defined between \f$\tau_s\f$ and \f$\tau_e\f$:
   * \f[
   \tau_s = \mathrm{Max}(t_p-\delta t/2, t_s)
   * \f]
   * \f[
   \tau_e = \mathrm{Min}(t_p+\delta t/2, t_e)
   * \f]
   * where \f$t_s\f$, \f$t_e\f$, and \f$t_p\f$ are the cluster start time, end time and peak time respectively. Two clusters are said to coincide if their effective durations overlap.
   * All coinc events are tagged to true.
   * @returns The number of coinc events.
   */
  unsigned int MakeCoinc(void);

  /**
   * @brief Prints the list of coinc events.
   */
  void PrintCoinc(void);

  /**
   * @brief Prints the list of active clusters NOT participating to a coinc event.
   * @note Not-active clusters are not printed.
   * @param[in] aSampleIndex Trigger sample index: 0 or 1.
   */
  void PrintCoincNot(const unsigned int aSampleIndex);

  /**
   * @brief Returns the coincidence time window start \f$\tau_s\f$[s].
   * @details The window start is defined as:
   * \f[
   \tau_s = \mathrm{Max}(t_p-\delta t/2, t_s)
   * \f]
   * where \f$t_s\f$  and \f$t_p\f$ are the cluster start time and peak time respectively.
   * @param[in] aSampleIndex Trigger sample index: 0 or 1.
   * @param[in] aClusterIndex Cluster index for the selected trigger sample.
   * @pre Both the sample index and cluster index must be valid!
   */
  inline double GetCoincTimeWindowStart(const unsigned int aSampleIndex, const unsigned int aClusterIndex){
    return TMath::Max(triggers[aSampleIndex]->GetClusterTime(aClusterIndex)+toffset[aSampleIndex]-coinc_dt/2.0,
                      triggers[aSampleIndex]->GetClusterTimeStart(aClusterIndex)+toffset[aSampleIndex]);
  };

  /**
   * @brief Returns the coincidence time window end \f$\tau_e\f$[s].
   * @details The window end is defined as:
   * \f[
   \tau_e = \mathrm{Min}(t_p+\delta t/2, t_e)
   * \f]
   * where \f$t_e\f$  and \f$t_p\f$ are the cluster end time and peak time respectively.
   * @param[in] aSampleIndex Trigger sample index: 0 or 1.
   * @param[in] aClusterIndex Cluster index for the selected trigger sample.
   * @pre Both the sample index and cluster index must be valid!
   */
  inline double GetCoincTimeWindowEnd(const unsigned int aSampleIndex, const unsigned int aClusterIndex){
    return TMath::Min(triggers[aSampleIndex]->GetClusterTime(aClusterIndex)+toffset[aSampleIndex]+coinc_dt/2.0,
                      triggers[aSampleIndex]->GetClusterTimeEnd(aClusterIndex)+toffset[aSampleIndex]);
  };

  /**
   * @brief Sets the chacteristic duration to define a coinc \f$\delta t\f$.
   * @param[in] aDeltat Chacteristic duration \f$\delta t\f$ [s].
   */
  inline void SetCoincDeltat(const double aDeltat){ coinc_dt=aDeltat; };

  /**
   * @brief Returns the current number of coinc events.
   */
  inline unsigned int GetCoincN(void){ return CoC[0].size(); };

  /**
   * @brief Returns the cluster index involved in a given coinc event.
   * @warning The validity of the coinc index is not checked.
   * @param[in] aSampleIndex Trigger sample index: 0 or 1.
   * @param[in] aCoincIndex Coinc index.
   */
  inline unsigned int GetClusterIndex(const unsigned int aSampleIndex, const unsigned int aCoincIndex){ 
    return CoC[aSampleIndex%COINC2_NT][aCoincIndex];
  };

  /**
   * @brief Sets a new tag to a given coinc event.
   * @warning The validity of the coinc index is not checked.
   * @param[in] aCoincIndex Coinc index.
   * @param[in] aNewTag New tag value.
   */
  inline void SetCoincTag(const unsigned int aCoincIndex, const bool aNewTag){ 
    CoTag[aCoincIndex]=aNewTag;
  };

  /**
   * @brief Sets a new tag for all coinc events.
   * @param[in] aNewTag New tag value.
   */
  inline void SetCoincTag(const bool aNewTag){ 
    for(unsigned int c=0; c<CoC[0].size(); c++) CoTag[c]=aNewTag;
  };

  /**
   * @brief Gets the tag value of a given coinc event.
   * @warning The validity of the coinc index is not checked.
   * @param[in] aCoincIndex Coinc index.
   */
  inline bool GetCoincTag(const unsigned int aCoincIndex){ return CoTag[aCoincIndex]; };

  /**
   * @brief Gets the number of active clusters.
   * @details Active clusters are used by the coincidence algorithm.
   * They are defined when calling SetTriggers().
   * @sa SelectClusters().
   * @param[in] aSampleIndex Trigger sample index: 0 or 1.
   */
  inline unsigned int GetActiveClusterN(const unsigned int aSampleIndex){ 
    return nactive[aSampleIndex%COINC2_NT];
  };

  /**
   * @brief Returns the time offset of a trigger set [s].
   * @param[in] aSampleIndex Trigger sample index: 0 or 1.
   */
  inline double GetTimeOffset(const unsigned int aSampleIndex){ 
    return toffset[aSampleIndex%COINC2_NT];
  };

  /**
   * @brief Returns the chacteristic duration defining a coinc \f$\delta t\f$ [s].
   */
  inline double GetCoincDeltat(void){ return coinc_dt; };

  /**
   * @brief Returns the livetime of coinc segments [s].
   */
  inline double GetCoincLiveTime(void){ return CoSeg->GetLiveTime(); };

  /**
   * @brief Returns a pointer to the coincidence segments.
   */
  inline Segments* GetCoincSegments(void){
    return CoSeg;
  };

  /**
   * @brief Returns a copy of coincidence segments.
   */
  inline Segments* GetCoincSegmentsCopy(void){
    return new Segments(CoSeg->GetStarts(), CoSeg->GetEnds());
  };

  /**
   * @brief Sets a minimum SNR to select input clusters.
   * @sa SelectClusters()
   * @param[in] aSampleIndex Trigger sample index.
   * @param[in] aSnrMin Minimum SNR.
   */
  inline void SetSnrMin(const unsigned int aSampleIndex, const double aSnrMin){ 
    snrmin[aSampleIndex%COINC2_NT]=aSnrMin;
    return;
  };

  /**
   * @brief Returns the minimum SNR used to select input clusters [Hz].
   * @param[in] aSampleIndex Trigger sample index.
   */
  inline double GetSnrMin(const unsigned int aSampleIndex){ 
    return snrmin[aSampleIndex%COINC2_NT];
  };

  /**
   * @brief Sets a frequency range to select input clusters.
   * @sa SelectClusters()
   * @param[in] aSampleIndex Trigger sample index.
   * @param[in] aFrequencyMin Minimum frequency [Hz].
   * @param[in] aFrequencyMax Maximum frequency [Hz].
   */
  inline void SetFrequencyRange(const unsigned int aSampleIndex, const double aFrequencyMin, const double aFrequencyMax){ 
    freqmin[aSampleIndex%COINC2_NT]=aFrequencyMin;
    freqmax[aSampleIndex%COINC2_NT]=aFrequencyMax;
    return;
  };

  /**
   * @brief Returns the minimum frequency used to select input clusters [Hz].
   * @param[in] aSampleIndex Trigger sample index.
   */
  inline double GetFrequencyMin(const unsigned int aSampleIndex){ 
    return freqmin[aSampleIndex%COINC2_NT];
  };

  /**
   * @brief Returns the miaximum frequency used to select input clusters [Hz].
   * @param[in] aSampleIndex Trigger sample index.
   */
  inline double GetFrequencyMax(const unsigned int aSampleIndex){ 
    return freqmax[aSampleIndex%COINC2_NT];
  };

  /**
   * @brief Produce plots for coinc events.
   * @details List of plots:
   * - Fraction of coincident events as a function of SNR and frequency
   * - SNR vs SNR for coinc events
   * - Frequency vs frequency for coinc events
   * - SNR vs time for clusters of trigger sample 0
   * - SNR vs time for clusters of trigger sample 1
   * - Frequency vs time for clusters of trigger sample 0
   * - Frequency vs time for clusters of trigger sample 1
   *
   * @warning Only valid coinc events are plotted.
   */
  bool MakeComparators(void);

  /**
   * @brief Prints plots for coinc events.
   * @details After calling MakeComparators(), the plots are printed in png files.
   * @param[in] aFileName Output file name. Do not provide the file extension, it will be automatically added. For example: `"/path/to/directory/filename"`.
   */
  void PrintComparators(const string aFileName);

 private:

  // GENERAL
  unsigned int verbosity;              ///< Verbosity level.

  // TRIGGERS
  ReadTriggers *triggers[COINC2_NT];   ///< Trigger sets.
  double toffset[COINC2_NT];           ///< Time offsets [s].
  unsigned int nactive[COINC2_NT];     ///< Number of active clusters.
  double snrmin[COINC2_NT];            ///< SNR min selection.
  double freqmin[COINC2_NT];           ///< Frequency min selection [Hz].
  double freqmax[COINC2_NT];           ///< Frequency max selection [Hz].

  // COINC
  double coinc_dt;                     ///< Coinc time distance [s].
  vector <unsigned int> CoC[COINC2_NT];///< Coinc cluster index.
  bool *CoTag;                         ///< Coinc tags.
  Segments *CoSeg;                     ///< Coinc segments.

  // COMPARATORS
  TH1D *hc_snrfrac[2];        ///< coinc fraction vs SNR
  TH1D *hc_freqfrac[2];       ///< coinc fraction vs frequency
  TGraph *gc_snrtime[4];      ///< SNR vs time.
  TGraph *gc_freqtime[4];     ///< Coinc Frequency vs time.
  TGraph *gc_snrsnr;          ///< Coinc SNR vs SNR.
  TGraph *gc_freqfreq;        ///< Frequency vs frequency.

  /**
   * @brief Select input clusters.
   * @details The input clusters are set to "active" if all the following condition are met:
   * - The cluster tag is positive or zero: clusters with a negative tag are ignored.
   * - The cluster time (after time offset) is inside the coinc segments.
   * - The cluster frequency is inside the frequency range defined with SetFrequencyRange().
   * - The cluster SNR is above the minimum defined with SetSnrMin().
   *
   * @warning The cluster tag is set to -1 if the time, frequency, or SNR selection fails.
   * @param[in] aSampleIndex Trigger sample index.
   */
  void SelectClusters(const unsigned int aSampleIndex);
  
  ClassDef(Coinc2,0)  
};

#endif


